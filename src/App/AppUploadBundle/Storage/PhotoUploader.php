<?php
namespace App\AppUploadBundle\Storage;

use Aws\Credentials\CredentialProvider;
use Aws\S3\S3Client;
use Gaufrette\Adapter\AwsS3;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\EntityManager;
use Gaufrette\Filesystem;
use Symfony\Component\HttpFoundation\Session\Session;
use Gaufrette\Adapter\Local as LocalAdapter;
use Gaufrette\Adapter\Cache as CacheAdapter;


class PhotoUploader
{
    private static $allowedMimeTypes = array(
        'image/jpeg',
        'image/png',
        'image/gif',
    );
    private $filesystem;
    private $em;


    public function __construct(Container $container)
    {
        define("AWS_CERTIFICATE_AUTHORITY", true);
        // On créé le client Amazon
        $this->client = S3Client::factory([
            'region' => 'eu-central-1',
            'version' => 'latest'
        ]);
        $this->container = $container;
    }


    public function ObtainFile($file, $dir, $titre)
    {
        $local = new LocalAdapter($dir, true);
        $fileSystem2 = New Filesystem($local);
        $fileSystem2->getAdapter($local);
        $adapter = $this->getAdapterS3(false);
        $file = $adapter->read( $file);
        $fileSystem2->write($titre , $file,777);
    }
    public function Delete($file)
    {
        $urlweb = explode('/thumb/', $file->getUrlweb());
        $urlweb = $urlweb[1];
        $filename = sprintf('%s', $urlweb);
        $adapter = $this->getAdapterS3(false);
        $adapter->delete($filename);
        $filename = sprintf('%s/%s', 'thumb', $urlweb);
        $adapter = $this->getAdapterS3(false);
        $adapter->delete($filename);
        $filename = sprintf('%s/%s', 'thumb2', $urlweb);
        $adapter->delete($filename);
        $filename = sprintf('%s/%s', 'low', $urlweb);
        $adapter->delete($filename);
        $filename = sprintf('%s/%s', 'medium', $urlweb);
        $adapter->delete($filename);
        $this->em->Remove($file);
        $this->em->flush();
    }
    public function upload(UploadedFile $file)
    {
        $adapter = $this->getAdapterS3(false);

        $mimeType = $file->getMimeType();
        $extension = explode('/',$mimeType)[1];

        $uniquetitle = uniqid();
        $fileurl = sprintf('%s/%s.%s','original', $uniquetitle, $extension);
        $fileurlBd = sprintf('%s.%s', $uniquetitle, $extension);
        $expires = new \DateTime();
        $expires->setTimestamp(time() + (86400 * 366 * 100)); // 100 ans, on a de la marge
        $meta = array(
            'Access-Control-Allow-Origin'     => '*',
            'Access-Control-Allow-Methods'     => 'POST, GET, OPTIONS, PUT, HEAD',
            'CacheControl'                    => 'max-age=864000, s-maxage=864000',
            'Expires'                        => $expires,
            'contentType' => $file->getMimeType()
        );
        //$this->Delete($file);
        $adapter->setMetadata($fileurl, $meta);
        $adapter->write($fileurl,file_get_contents($file->getPathname()));



        $this->resize('white',$file->getPathname(), 'assets/resizer/thumb1.jpg', 600, 600);

        $imagenew = new UploadedFile('assets/resizer/thumb1.jpg', $file->getPathname(), 'image/jpeg');

        $fileurl = sprintf('%s/%s.%s','600x600', $uniquetitle, $extension);
        $adapter->setMetadata($fileurl, $meta);
        $adapter->write($fileurl,file_get_contents($imagenew->getPathname()));


        $this->resize('home',$file->getPathname(), 'assets/resizer/thumb1.jpg', 180, 180);

        $imagenew = new UploadedFile('assets/resizer/thumb1.jpg', $file->getPathname(), 'image/jpeg');

        $fileurl = sprintf('%s/%s.%s','180x180', $uniquetitle, $extension);
        $adapter->setMetadata($fileurl, $meta);
        $adapter->write($fileurl,file_get_contents($imagenew->getPathname()));

        $this->resize('white',$file->getPathname(), 'assets/resizer/thumb1.jpg', 70, 70);

        $imagenew = new UploadedFile('assets/resizer/thumb1.jpg', $file->getPathname(), 'image/jpeg');

        $fileurl = sprintf('%s/%s.%s','70x70', $uniquetitle, $extension);
        $adapter->setMetadata($fileurl, $meta);
        $adapter->write($fileurl,file_get_contents($imagenew->getPathname()));


        $return['type'] = $mimeType;
        $return['url'] = $fileurlBd;

        return $return;

    }


    function resize($color,$source_image, $destination, $tn_w, $tn_h, $quality = 80, $wmsource = false)
    {
        if($color == 'white'){
            $color = 255;
        }else{
            $color = 247;
        }

        $info = getimagesize($source_image);
        $imgtype = image_type_to_mime_type($info[2]);
        #assuming the mime type is correct
        switch ($imgtype) {
            case 'image/jpeg':
                $source = imagecreatefromjpeg($source_image);
                break;
            case 'image/gif':
                $source = imagecreatefromgif($source_image);
                break;
            case 'image/png':
                $source = imagecreatefrompng($source_image);
                break;
            default:
                die('Invalid image type.');
        }
        #Figure out the dimensions of the image and the dimensions of the desired thumbnail
        $src_w = imagesx($source);
        $src_h = imagesy($source);
        #Do some math to figure out which way we'll need to crop the image
        #to get it proportional to the new size, then crop or adjust as needed
        $x_ratio = $tn_w / $src_w;
        $y_ratio = $tn_h / $src_h;
        if (($src_w <= $tn_w) && ($src_h <= $tn_h)) {
            $new_w = $src_w;
            $new_h = $src_h;
        } elseif (($x_ratio * $src_h) < $tn_h) {
            $new_h = ceil($x_ratio * $src_h);
            $new_w = $tn_w;
        } else {
            $new_w = ceil($y_ratio * $src_w);
            $new_h = $tn_h;
        }
        $newpic = imagecreatetruecolor(round($new_w), round($new_h));
        imagecopyresampled($newpic, $source, 0, 0, 0, 0, $new_w, $new_h, $src_w, $src_h);
        $final = imagecreatetruecolor($tn_w, $tn_h);
        $backgroundColor = imagecolorallocate($final, $color, $color, $color);
        imagefill($final, 0, 0, $backgroundColor);
        //imagecopyresampled($final, $newpic, 0, 0, ($x_mid - ($tn_w / 2)), ($y_mid - ($tn_h / 2)), $tn_w, $tn_h, $tn_w, $tn_h);
        imagecopy($final, $newpic, (($tn_w - $new_w)/ 2), (($tn_h - $new_h) / 2), 0, 0, $new_w, $new_h);
        #if we need to add a watermark
        if ($wmsource) {
            #find out what type of image the watermark is
            $info    = getimagesize($wmsource);
            $imgtype = image_type_to_mime_type($info[2]);
            #assuming the mime type is correct
            switch ($imgtype) {
                case 'image/jpeg':
                    $watermark = imagecreatefromjpeg($wmsource);
                    break;
                case 'image/gif':
                    $watermark = imagecreatefromgif($wmsource);
                    break;
                case 'image/png':
                    $watermark = imagecreatefrompng($wmsource);
                    break;
                default:
                    die('Invalid watermark type.');
            }
            #if we're adding a watermark, figure out the size of the watermark
            #and then place the watermark image on the bottom right of the image
            $wm_w = imagesx($watermark);
            $wm_h = imagesy($watermark);
            imagecopy($final, $watermark, $tn_w - $wm_w, $tn_h - $wm_h, 0, 0, $tn_w, $tn_h);
        }
        if (imagejpeg($final, $destination, $quality)) {
            return true;
        }
        return false;
    }

    private function getAdapterS3($create = true, $acl = 'public-read')
    {
        // On génère l'adapter avec le client d'amazon
        return new AwsS3(
            $this->client,
            $this->container->getParameter('amazon_s3_bucket_name'),
            [
                'create' => $create,
                'acl' => $acl
            ]
        );
    }
}
?>
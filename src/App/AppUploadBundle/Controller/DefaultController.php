<?php

namespace  App\AppUploadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AppUploadBundle:Default:index.html.twig');
    }
}

<?php

namespace App\VideoFrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('VideoFrontBundle:Home:index.html.twig');
    }
}

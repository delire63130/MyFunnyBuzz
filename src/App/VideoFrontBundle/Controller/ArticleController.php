<?php

namespace App\VideoFrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ArticleController extends Controller
{
    public function indexAction()
    {
        return $this->render('VideoFrontBundle:Article:index.html.twig');
    }
}

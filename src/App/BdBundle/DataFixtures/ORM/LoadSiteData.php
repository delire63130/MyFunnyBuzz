<?php
/**
 * Fichier des triggers et fonctions SQL.
 *
 * @ignore
 */

namespace DataFixtures\ORM;

use App\BdBundle\Entity\Category;
use App\BdBundle\Entity\Site;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;


/**
 * Classe LoadSiteData.
 *
 * @ignore
 */
class LoadSiteData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {




        $data = [
            [
                'name' => 'MinuteBuzz.com',
                'url' => 'http://www.minutebuzz.com/',
                'parser' => '\Utils\Parser\MinuteBuzzParser',
                'categories' => [
                    [
                        'label' => Category::LOL,
                        'url' => 'http://www.minutebuzz.com/webservice/ajaxmore/?type=emotion&search=emo_1&start=0&nb=18',
                        'format' => 'json'
                    ],[
                        'label' => Category::WIN,
                        'url' => 'http://www.minutebuzz.com/webservice/ajaxmore/?type=emotion&search=emo_2&start=0&nb=18',
                        'format' => 'json'
                    ],[
                        'label' => Category::FAIL,
                        'url' => 'http://www.minutebuzz.com/webservice/ajaxmore/?type=emotion&search=emo_3&start=0&nb=18',
                        'format' => 'json'
                    ],[
                        'label' => Category::CUTE,
                        'url' => 'http://www.minutebuzz.com/webservice/ajaxmore/?type=emotion&search=emo_4&start=0&nb=18',
                        'format' => 'json'
                    ],[
                        'label' => Category::WTF,
                        'url' => 'http://www.minutebuzz.com/webservice/ajaxmore/?type=emotion&search=emo_5&start=0&nb=18',
                        'format' => 'json'
                    ],[
                        'label' => Category::CHOC,
                        'url' => 'http://www.minutebuzz.com/webservice/ajaxmore/?type=emotion&search=emo_6&start=0&nb=18',
                        'format' => 'json'
                    ],[
                        'label' => Category::OMG,
                        'url' => 'http://www.minutebuzz.com/webservice/ajaxmore/?type=emotion&search=emo_7&start=0&nb=18',
                        'format' => 'json'
                    ],[
                        'label' => Category::HOT,
                        'url' => 'http://www.minutebuzz.com/webservice/ajaxmore/?type=emotion&search=emo_8&start=0&nb=18',
                        'format' => 'json'
                    ]
                ]
            ],[
                'name' => 'BuzzFeed.com',
                'url' => 'http://www.buzzfeed.com/',
                'parser' => '\Utils\Parser\BuzzFeedParser',
                'categories' => [
                    [
                        'label' => Category::LOL,
                        'url' => 'http://www.buzzfeed.com/lol-fr',
                        'format' => 'dom'
                    ],[
                        'label' => Category::WIN,
                        'url' => 'http://www.buzzfeed.com/win-fr',
                        'format' => 'dom'
                    ],[
                        'label' => Category::OMG,
                        'url' => 'http://www.buzzfeed.com/omg-fr',
                        'format' => 'dom'
                    ],[
                        'label' => Category::CUTE,
                        'url' => 'http://www.buzzfeed.com/cute-fr',
                        'format' => 'dom'
                    ],[
                        'label' => Category::FAIL,
                        'url' => 'http://www.buzzfeed.com/fail-fr',
                        'format' => 'dom'
                    ],[
                        'label' => Category::WTF,
                        'url' => 'http://www.buzzfeed.com/wtf-fr',
                        'format' => 'dom'
                    ],[
                        'label' => Category::HOT,
                        'url' => 'http://www.buzzfeed.com/hot-fr',
                        'format' => 'dom'
                    ]
                ]
            ],[
                'name' => 'Spi0n.com',
                'url' => 'http://www.spi0n.com/',
                'parser' => '\Utils\Parser\SpionParser',
                'categories' => [
                    [
                        'label' => Category::LOL,
                        'url' => 'http://www.spi0n.com/humour-comedie/',
                        'format' => 'html'
                    ],[
                        'label' => Category::CHOC,
                        'url' => 'http://www.spi0n.com/trash-gore/',
                        'format' => 'html'
                    ],[
                        'label' => Category::WTF,
                        'url' => 'http://www.spi0n.com/wtf/',
                        'format' => 'html'
                    ]
                ]
            ],[
                'name' => 'Mega-Buzz.com',
                'url' => 'http://www.mega-buzz.com/',
                'parser' => '\Utils\Parser\MegaBuzzParser',
                'categories' => [
                    [
                        'label' => Category::LOL,
                        'url' => 'http://www.mega-buzz.com/buzz-video-lol',
                        'format' => 'html'
                    ],[
                        'label' => Category::WIN,
                        'url' => 'http://www.mega-buzz.com/buzz-video-win',
                        'format' => 'html'
                    ],[
                        'label' => Category::OMG,
                        'url' => 'http://www.mega-buzz.com/buzz-video-omg',
                        'format' => 'html'
                    ],[
                        'label' => Category::WTF,
                        'url' => 'http://www.mega-buzz.com/buzz-video-wtf',
                        'format' => 'html'
                    ],[
                        'label' => Category::FAIL,
                        'url' => 'http://www.mega-buzz.com/buzz-video-fail',
                        'format' => 'html'
                    ],[
                        'label' => Category::CUTE,
                        'url' => 'http://www.mega-buzz.com/buzz-video-cute',
                        'format' => 'html'
                    ],[
                        'label' => Category::CHOC,
                        'url' => 'http://www.mega-buzz.com/buzz-video-choc',
                        'format' => 'html'
                    ],[
                        'label' => Category::HOT,
                        'url' => 'http://www.mega-buzz.com/buzz-video-hot',
                        'format' => 'html'
                    ]
                ]
            ]
        ];

        foreach($data as $d)
        {
            $site = new Site();
            $site->setName( $d['name'] );
            $site->setUrl( $d['url'] );
            $site->setParser( $d['parser'] );

            foreach($d['categories'] as $c)
            {
                $categ = new Category();
                $categ->setLabel( $c['label'] );
                $categ->setUrl( $c['url'] );
                $categ->setFormat( $c['format'] );

                $categ->setSite( $site );
                $site->addCategory( $categ );
            }

            $manager->persist($site);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
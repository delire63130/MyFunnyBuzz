<?php

namespace App\BdBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * LikeJacker
 *
 * @ORM\Table(name="like_jacker")
 * @ORM\Entity(repositoryClass="App\BdBundle\Repository\LikeJackerRepository")
 */
class LikeJacker
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStart", type="datetime")
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateStop", type="datetime")
     */
    private $dateStop;

    /**
     * @var integer
     *
     * @ORM\Column(name="percentLike", type="integer")
     */
     private $percentLike;

    /**
     * @var Page
     *
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="likeJackers", fetch="EAGER")
     * @ORM\JoinColumn(name="Article_id", referencedColumnName="id")
     */
    private $article;

    /**
     *
     * @ORM\Column(name="isOff", type="boolean", nullable=false)
     */
    protected $isOff;

    /**
     *
     * @ORM\Column(name="isPub", type="boolean")
     */
    protected $isPubOn;

    /**
     * @return mixed
     */
    public function getIsPubOn()
    {
        return $this->isPubOn;
    }

    /**
     * @param mixed $isPubOn
     */
    public function setIsPubOn($isPubOn)
    {
        $this->isPubOn = $isPubOn;
    }

    /**
     * @return mixed
     */
    public function getIsOff()
    {
        return $this->isOff;
    }

    /**
     * @param mixed $isOff
     */
    public function setIsOff($isOff)
    {
        $this->isOff = $isOff;
    }

    public function __construct()
    {
        $this->isOff = false;
        $this->isPubOn = false;
        $this->dateStart = new \DateTime();
        $this->dateStop = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getPercentLike()
    {
        return $this->percentLike;
    }

    /**
     * @param mixed $percentLike
     */
    public function setPercentLike($percentLike)
    {
        $this->percentLike = $percentLike;
    }

    /**
     * @return Page
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param Page $article
     */
    public function setArticle($article)
    {
        $this->article = $article;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return LikeJacker
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateStop
     *
     * @param \DateTime $dateStop
     *
     * @return LikeJacker
     */
    public function setDateStop($dateStop)
    {
        $this->dateStop = $dateStop;

        return $this;
    }

    /**
     * Get dateStop
     *
     * @return \DateTime
     */
    public function getDateStop()
    {
        return $this->dateStop;
    }
}


<?php

namespace App\BdBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Page
 * @UniqueEntity("nom",message="Le nom de la page est déjà utilisé")
 * @UniqueEntity("domaine",message="Le domaine est déjà utilisé pour une autre page")
 * @UniqueEntity("url",message="Url déjà utilisée pour une autre page")
 * @UniqueEntity("idPage",message="L'id de la page déjà utilisé")
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\BdBundle\Repository\PageRepository")
 */
class Page
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var string
     * @Assert\NotNull(message="Veuillez renseigner le nom de la page")
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $nom;

    /**
     * @var Account
     * @Assert\NotNull(message="Veuillez renseigner le compte parent de la page")
     * @ORM\ManyToOne(targetEntity="Account", inversedBy="pages", fetch="EAGER")
     * @ORM\JoinColumn(name="Account_id", referencedColumnName="id")
     */
    private $account;


    /**
     * @var string
     *
     * @ORM\Column(name="domaine", type="string", length=255, nullable=true)
     */
        private $domaine;



    /**
     * @var string
     * @Assert\NotNull(message="Veuillez renseigner l'url de la page")
     * @ORM\Column(name="Url", type="string", length=255)
     */
    private $url;


    /**
     * @var ArrayCollection
     *
    * @ORM\ManyToMany(targetEntity="Publi", mappedBy="pages")
     */
    private $publis;


    /**
     * @var integer
     * @Assert\NotNull(message="Veuillez renseigner l'id de la page")
     * @ORM\Column(name="id_page", type="bigint", unique=true)
     */
    private $idPage;

    /**
     * @var integer
     * @ORM\Column(name="nb_likejack", type="integer", unique=false, nullable=true)
     */
    private $nbLikeJack;

    /**
     * @return int
     */
    public function getNbLikeJack()
    {
        return $this->nbLikeJack;
    }

    /**
     * @param int $nbLikeJack
     */
    public function setNbLikeJack($nbLikeJack)
    {
        $this->nbLikeJack = $nbLikeJack;
    }


    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url){
        $this->url = $url;
    }


    public function __Construct(){
        $this->publis = new ArrayCollection();
    }


    /**
     * @return ArrayCollection
     */
    public function getPublis()
    {
        return $this->publis;
    }



    public function addPubli(Publi $publi)
    {
        if(!$this->publis->contains($publi))
        {
            $this->publis[] = $publi;
        }
    }

    public function removePubli(Publi $publi)
    {
        $this->publis->removeElement($publi);
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Page
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }



    /**
     * @param int $nbLikeDay
     */
    public function setNbLikeDay($nbLikeDay)
    {
        $this->nbLikeDay = $nbLikeDay;
    }

    /**
     * @return int
     */
    public function getIdPage()
    {
        return $this->idPage;
    }

    /**
     * @param int $idPage
     */
    public function setIdPage($idPage)
    {
        $this->idPage = $idPage;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Category $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getDomaine()
    {
        return $this->domaine;
    }

    /**
     * @param string $domaine
     */
    public function setDomaine($domaine)
    {
        $this->domaine = $domaine;
    }

    public function __toString()
    {
        return $this->nom;
    }


}

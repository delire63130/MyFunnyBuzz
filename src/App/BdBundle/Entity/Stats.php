<?php

namespace App\BdBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stats
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\BdBundle\Repository\StatsRepository")
 */
class Stats
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var integer
     *
     * @ORM\Column(name="nbView", type="integer")
     */
    private $nbView;


    /**
     * @var integer
     *
     * @ORM\Column(name="rating", type="integer")
     */
    private $rating;

    public function __construct()
    {
        $this->nbView=0;
        $this->rating = 0;
    }

    /**
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }






    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nbView
     *
     * @param integer $nbView
     * @return Stats
     */
    public function setNbView($nbView)
    {
        $this->nbView = $nbView;

        return $this;
    }

    /**
     * Get nbView
     *
     * @return integer 
     */
    public function getNbView()
    {
        return $this->nbView;
    }
}

<?php

namespace App\BdBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Account
 * @UniqueEntity("token",message="Token déjà utilisé sur un autre compte")
 * @UniqueEntity("app_secret",message="Mot de passe de l'application déjà utilisé sur un autre compte")
 * @UniqueEntity("app_id",message="Mot de passe de l'application déjà utilisé sur un autre compte")
 * @UniqueEntity("app_secret",message="L'id de l'application déjà utilisé sur un autre compte")
 * @UniqueEntity("mail",message="L'adresse email est déjà utilisée sur un autre compte")
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\BdBundle\Repository\AccountRepository")
 */
class Account
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=true)
     */
    private $token;

     /**
     * @var string
      * @Assert\NotNull(message="Le mot de passe de l'application facebook ne peut etre nul")
     * @ORM\Column(name="app_secret", type="string", length=255, nullable=true)
     */
    private $app_secret;

    /**
     * @var string
     * @Assert\NotNull(message="L'id de l'app ne peut pas être nul")
     * @ORM\Column(name="app_id", type="string", length=255, nullable=true)
     */
    private $app_id;


    /**
     * @var string
     * @Assert\NotNull(message="Le nom de l'utilisateur du compte ne peut pas etre nul")
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Assert\NotNull(message="Le prénom de l'utilisateur du compte ne peut pas etre nul")
     * @ORM\Column(name="surname", type="string", length=255)
     */
    private $surname;


    /**
     * @var string
     * @Assert\NotNull(message="Le mot de passe de l'utilisateur du compte ne peut pas etre nul")
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;


    /**
     * @var string
     * @Assert\NotNull(message="Le mail de l'utilisateur du compte ne peut pas etre nul")
     * @ORM\Column(name="mail", type="string", length=255)
     */
    private $mail;


    /**
     * @var Article
     *
     * @ORM\OneToMany(targetEntity="Page", mappedBy="account")
     */
    private $pages;

    public function __toString()
    {
        return $this->mail;
    }

    public function __construct()
    {
        $this->pages = new ArrayCollection();
    }

    public function addPage(Page $page){
        if(!$this->pages->contains($page)){
            $this->pages->add($page);
        }
    }

    public function removePage(Page $page){
        if($this->pages->contains($page)){
            $this->pages->removeElement($page);
        }
    }

    public function getPages(){
        return $this->pages;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     * @return Account
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return Account
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Account
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return Account
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @return string
     */
    public function getAppSecret()
    {
        return $this->app_secret;
    }

    /**
     * @param string $app_secret
     */
    public function setAppSecret($app_secret)
    {
        $this->app_secret = $app_secret;
    }

    /**
     * @return string
     */
    public function getAppId()
    {
        return $this->app_id;
    }

    /**
     * @param string $app_id
     */
    public function setAppId($app_id)
    {
        $this->app_id = $app_id;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }
}

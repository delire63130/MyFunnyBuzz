<?php

namespace App\BdBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContext;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Publi
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\BdBundle\Repository\PubliRepository")
 */
class Publi
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotNull(message="Veuillez renseigner le texte de la publi")
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;


    /**
     * @var Article
     *
     * @ORM\ManyToOne(targetEntity="Article", fetch="EAGER")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     */
    private $article;

    /**
     *
     * @ORM\Column(name="isSentFacebook", type="boolean", nullable=false, options={"default":false})
     */
    protected $isSentFacebook;

    /**
     * @var Page
     *
     * @ORM\ManyToMany(targetEntity="Page", inversedBy="publis", fetch="EAGER")
     * @ORM\JoinTable(
     *        name="Publi_has_Page",
     *        joinColumns={
     *     		@ORM\JoinColumn(name="Publi_id", referencedColumnName="id", onDelete="CASCADE")
     *        },
     *      inverseJoinColumns = {
     *     		@ORM\JoinColumn(name="Page_id", referencedColumnName="id")
     *        }
     * )
     */
    private $pages;

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }


    public function __construct()
    {
        $this->isSentFacebook = false;
        $this->pages = new ArrayCollection();
        $this->date = new \DateTime();
        $this->date->modify('+1 hour');
    }

    public function getPage()
    {
        return $this->pages;
    }

    public function addPage(Page $page)
    {
        if (!$this->pages->contains($page)) {
            $this->pages->add($page);
        }
    }

    public function removePage(Page $page)
    {
        if ($this->pages->contains($page)) {
            $this->pages->removeElement($page);
        }
    }


    /**
     * @return Article
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param Article $article
     */
    public function setArticle(Article $article)
    {
        $this->article = $article;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Publi
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }


    /**
     * @return mixed
     */
    public function getIsSentFacebook()
    {
        return $this->isSentFacebook;
    }

    /**
     * @param mixed $isSentFacebook
     */
    public function setIsSentFacebook($isSentFacebook)
    {
        $this->isSentFacebook = $isSentFacebook;
    }

    public function getPages(){
        return $this->pages;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if ($this->pages->count()==0) {
            $context->addViolation('Veuillez au moins sélectionner une page!', array(), null);
        }
    }


}

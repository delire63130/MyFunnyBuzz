<?php

namespace App\BdBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Article
 * @UniqueEntity("url",message="Cette url est déjà utilisée pour un autre article")
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\BdBundle\Repository\ArticleRepository")
 */
class Article
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotNull(message="Le titre de l'article ne peut pas être null")
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     * @Assert\NotNull(message="Le contenu de l'article ne peut pas etre null")
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var string
     * @Assert\NotNull(message="La description de l'article ne peut pas etre null")
     * @ORM\Column(name="description", type="text")
     */
    private $description;


    /**
     * @var string
     * @Assert\NotNull(message="L'url de l'article ne peut pas etre null")
     * @ORM\Column(name="url", type="string",length=255, unique=true)
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Images")
     * @ORM\JoinColumn(nullable=false)
     */
    private $imageCover;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToOne(targetEntity="Stats", cascade={"all"}, fetch="EAGER")
     * @ORM\JoinColumn(name="stat_id", referencedColumnName="id")
     */
    private $stats;

    /**
     * @var ArrayCollection
    /**
     * @var Article
     *
     * @ORM\OneToMany(targetEntity="LikeJacker", mappedBy="article", fetch="EAGER")
     */
    private $likeJackers;


    /**
     * Constructeur de l'entité
     */
    public function __construct()
    {
        $stats = new Stats();
        $this->stats = $stats;
        $this->likeJackers = new ArrayCollection();
    }

    public function getLikeJackers(){
        return $this->likeJackers;
    }

    public function addLikeJacker(LikeJacker $likeJacker){
        if(!$this->likeJackers->contains($likeJacker)){
            $this->likeJackers->add($likeJacker);
        }
    }

    public function removeLikeJacker(LikeJacker $likeJacker){
        if($this->likeJackers->contains($likeJacker)){
            $this->likeJackers->removeElement($likeJacker);
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Article
     */
    public function setContent($content)
    {
        $this->content = $content;

    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }


    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }


    /**
     * @return ArrayCollection
     */
    public function getStats()
    {
        return $this->stats;
    }


    public function setStats(Stats $stats)
    {
        $this->stats = $stats;
    }


    /**
     * @return string
     */
    public function getImageCover()
    {
        return $this->imageCover;
    }

    /**
     * @param string $imageCover
     */
    public function setImageCover($imageCover)
    {
        $this->imageCover = $imageCover;
    }


    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function __toString()
    {
        return $this->title;
    }


}


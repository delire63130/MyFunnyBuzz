<?php

namespace App\BdBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ArticleRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ImageRepository extends EntityRepository
{
    public function findByMd5List(array $md5)
    {
        return $this
            ->createQueryBuilder('img')

            ->where('img.md5 IN (:md5)')

            ->setParameter(':md5', $md5)

            ->getQuery()
            ->getArrayResult()
            ;
    }
}


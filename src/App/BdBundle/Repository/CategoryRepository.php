<?php

namespace App\BdBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * CategoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CategoryRepository extends EntityRepository
{
    public function findAllArray(){
         return $this
            ->createQueryBuilder('cat')
             ->groupBy('cat.label')
            ->getQuery()
            ->getArrayResult();
    }

    public function findAndCount(){
        return $this
            ->createQueryBuilder('cat')
            ->getQuery()
            ->getArrayResult();
    }

    public function findAllUnique(){
        return $this
            ->createQueryBuilder('cat')
            ->select('cat.label')
            ->groupBy('cat.label')
            ->getQuery()
            ->getArrayResult();
    }

}

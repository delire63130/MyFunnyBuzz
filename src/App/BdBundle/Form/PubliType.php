<?php

namespace App\BdBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PubliType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('text', TextType::class, array(
            'label' => 'Texte de la publi',
            'attr' => array('class' => 'form-control')
        ))
            ->add('date', null, array(
                'label' => 'Date de publication'
            ))
            ->add('article', EntityType::class, array(
                'class' => 'AppBdBundle:Article',
                'attr' => array('class' => 'bootstrap-select select-picker form-control'),
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('art')
                        ->orderBy('art.date', 'DESC');
                },
                'label' => 'Article à publié',
            ))
            ->add('pages', null, array(
                'label' => 'Pages Concernées',
                'attr' => array('class' => 'bootstrap-select select-picker form-control')
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\BdBundle\Entity\Publi'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_bdbundle_publi';
    }


}

<?php

namespace App\BdBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LikeJackerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateStart', null, array(
                'label'=>'Date de début'
            ))
            ->add('dateStop', null, array(
                'label'=>'Date de fin'
            ))
            ->add('percentLike', IntegerType::class,array(
                'label'=>'Pourcentage de LikeJacking',
                'attr'=>array('class'=>'form-control')))
            ->add('article', null, array(
                'label'=>'Article likejacké',
                'attr'=>array('class'=>'form-control')))

        ->add('isOff',null, array('label'=>'Likejack Désctivé'))
        ->add('isPubOn',null, array('label'=>'Publicitée désactivée'));

    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\BdBundle\Entity\LikeJacker'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_bdbundle_likejacker';
    }


}

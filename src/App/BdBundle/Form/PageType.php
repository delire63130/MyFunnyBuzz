<?php

namespace App\BdBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom', TextType::class, array(
            'label' => 'Nom de la page',
            'attr' => array('class' => 'form-control')
        ))
            ->add('domaine', TextType::class, array(
                'label' => 'Domaine Likejacking',
                'attr' => array('class' => 'form-control')
            ))
           ->add('url', TextType::class, array(
               'label' => 'Url de la page',
               'attr' => array('class' => 'form-control')
           ))
            ->add('idPage', TextType::class, array(
                'label' => 'Id de la page',
                'attr' => array('class' => 'form-control')
            ))
            ->add('account', EntityType::class, array(
                'class'=> 'AppBdBundle:Account',
                'label' => 'Compte utilisateur de la page',
                'attr' => array('class' => 'form-control')
            ))        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\BdBundle\Entity\Page'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_bdbundle_page';
    }


}

<?php

namespace App\BdBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccountType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('name',TextType::class, array(
                'label'=>'Nom du compte',
                'attr'=>array('class'=>'form-control')
            ))

            ->add('surname',TextType::class, array(
                'label'=>'Prénom du compte',
                'attr'=>array('class'=>'form-control')
            ))

            ->add('password', TextType::class, array(
                'label'=>'Mot de passe du compte',
                'attr'=>array('class'=>'form-control')
            ))
            ->add('mail',TextType::class, array(
                'label'=>'Adresse email du compte',
                'attr'=>array('class'=>'form-control')
            ))

            ->add('app_secret',TextType::class,array(
                'label'=>'Mot de passe App Facebook',
                'attr'=>array('class'=>'form-control')
            ))

            ->add('token',TextType::class,array(
                'label'=>'Token App Facebook',
                'attr'=>array('class'=>'form-control')
            ))




            ->add('app_id',TextType::class,array(
                'label'=>'Identifiant de l\'app Facebook',
                'attr'=>array('class'=>'form-control')
            ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\BdBundle\Entity\Account'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_bdbundle_account';
    }


}

<?php

namespace App\BdBundle\Form;

use Doctrine\ORM\EntityRepository;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VideoType extends AbstractType
{
    private $date;
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        if($options['data']->getDate()!=null){
            $this->date = $options['data']->getDate();
        };

        $builder->add('title', TextType::class, array(
            'label' => 'Titre de l\'article',
            'attr' => array('class' => 'form-control')
        ))
            ->add('content', CKEditorType::class, array(
                'config_name' => 'my_config',
                'label' => 'Contenu de l\'article',
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'Description de l\'article',
                'attr' => array('class' => 'form-control')
            ))
            ->add('url', TextType::class, array(
                'label' => 'Url de l\'article',
                'attr' => array('class' => 'form-control')
            ))
            ->add('imageCover', EntityType::class, array(
                'class' => 'AppBdBundle:Images',
                'label'=>'Photo de couverture',
                'query_builder' => function (EntityRepository $er) {
                    if($this->date == null){
                        return $er->createQueryBuilder('img')
                            ->orderBy('img.dateAjout', 'DESC')
                            ->setMaxResults(16);
                    }else{
                        return $er->createQueryBuilder('img')
                            ->orderBy('img.dateAjout', 'DESC')
                            ->setMaxResults(16)
                            ->where('img.dateAjout<:date')
                            ->setParameter(':date',$this->date);
                    }
                },
                'choice_attr' => function($val, $key, $index) {
                    if($val != null){
                        // adds a class like attending_yes, attending_no, etc
                        return ['data-img-src' => 'https://s3.eu-central-1.amazonaws.com/image-myfunnybuzz/180x180/' . $val->getUrl()];
                    }
                },
                'choice_label' => function ($img) {
                    if($img != null) {
                        return 'https://s3.eu-central-1.amazonaws.com/image-myfunnybuzz/180x180/' . $img->getUrl();
                    }
                },
                'choice_value' => function ($img) {
                    if($img != null){
                        return 'https://s3.eu-central-1.amazonaws.com/image-myfunnybuzz/180x180/'.$img->getUrl();
                    }
                }))

            ->add('imageEditor', EntityType::class, array(
                'mapped'=>false,
                'class' => 'AppBdBundle:Images',
                'label'=>'Ajouter des images à l\'éditeur',
                'query_builder' => function (EntityRepository $er) {
                    if($this->date == null){
                        return $er->createQueryBuilder('img')
                            ->orderBy('img.dateAjout', 'DESC')
                            ->setMaxResults(16);
                    }else{
                        return $er->createQueryBuilder('img')
                            ->orderBy('img.dateAjout', 'DESC')
                            ->setMaxResults(16)
                            ->where('img.dateAjout<:date')
                            ->setParameter(':date',$this->date);
                    }
                },
                'choice_attr' => function($val, $key, $index) {
                    if($val != null){
                        // adds a class like attending_yes, attending_no, etc
                        return ['data-img-src' => 'https://s3.eu-central-1.amazonaws.com/image-myfunnybuzz/180x180/' . $val->getUrl()];
                    }
                },
                'choice_label' => function ($img) {
                    if($img != null) {
                        return 'https://s3.eu-central-1.amazonaws.com/image-myfunnybuzz/180x180/' . $img->getUrl();
                    }
                },
                'choice_value' => function ($img) {
                    if($img != null){
                        return 'https://s3.eu-central-1.amazonaws.com/image-myfunnybuzz/180x180/'.$img->getUrl();
                    }
                }));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\BdBundle\Entity\Video'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_bdbundle_video';
    }


}

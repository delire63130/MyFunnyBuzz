<?php
/**
 * Created by PhpStorm.
 * User: didi
 * Date: 26/11/15
 * Time: 14:26
 */
namespace App\AdminBundle\Utils;
use Symfony\Component\Form\Form;

class FormUtils
{
    /**
     * Permet de récupérer la liste des erreurs d'un formulaire Symfony.
     *
     * @param Form 		$form 		Le formulaire à parser
     * @param array 	$errors 	Le tableau associatif des erreurs (utilisé pour la redondance)
     * @param string 	$namePrefix Le préfixe du nom du champ (utilisé pour la redondance)
     *
     * @return array|null
     */
    public static function getErrorMessages(Form $form, $errors = null, $namePrefix = "")
    {
        if(is_null($errors))
        {
            $errors = array();
        }
        // Le nom du formulaire formulaire
        $formName = !empty($namePrefix) ? $namePrefix.'_' : '';
        $formName .= $form->getName();
        // Les erreurs propres au formulaire
        foreach ($form->getErrors() as $key => $error)
        {
            $template = $error->getMessageTemplate();
            $parameters = $error->getMessageParameters();
            foreach ($parameters as $var => $value)
            {
                $template = str_replace($var, $value, $template);
            }
            $errors[$formName ."_". $key] = $template;
        }
        // Si le formulaire à des enfants
        if ($form->count())
        {
            // On parcourt la liste des enfants (qui sont eux aussi des formulaires)
            foreach ($form as $child)
            {
                if (!$child->isValid())
                {
                    // Si l'enfant a des enfants
                    if ($child->count())
                    {
                        $errors = self::getErrorMessages($child, $errors, $formName);
                    }
                    else
                    {
                        $fieldName = $formName .'_'. $child->getName();
                        $childErrors = self::getErrorMessages($child);
                        if(is_array($childErrors) && count($childErrors) == 1)
                        {
                            $first = array_keys($childErrors)[0];
                            $childErrors = $childErrors[$first];
                        }
                        $errors[$fieldName] = $childErrors;
                    }
                }
            }
        }
        return $errors;
    }
}

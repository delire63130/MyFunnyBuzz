<?php

namespace App\AdminBundle\Controller;

use App\AdminBundle\Utils\FormUtils;
use App\BdBundle\Entity\Images;
use App\BdBundle\Entity\LikeJacker;
use App\BdBundle\Entity\Publi;
use App\BdBundle\Form\LikeJackerType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use App\AppUploadBundle\PhotoUploader;

class LikeJackingController extends Controller
{
    public function indexAction(){
       $likeJackers = $this->getLikeJackRepo()->findAll();
        return $this->render('AppAdminBundle:LikeJacking:index.html.twig', array('likJakers'=>$likeJackers));
    }

    public function editAction(LikeJacker $likejacker, Request $request){

        $form = $this->createForm(LikeJackerType::class, $likejacker);

        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            return $this->render('AppAdminBundle:LikeJacking:add.html.twig',array('form'=>$form->createView()));
        } else {
            $form->handleRequest($request);

            // Si le formulaire est valide
            if ($form->isValid()) {
                $em = $this->container->get("doctrine.orm.default_entity_manager");
                $form = $form->getData();

                $json['submitted'] = true;

                $em->persist($form);
                $em->flush();
                // La redirection
                $json['redirect'] = $this->get('router')->generate('likejacking_gestion');

            } else {

                $json['errors'] = array(
                    'symfony' => FormUtils::getErrorMessages($form)
                );

            }

            return new JsonResponse($json);
        }
    }
    public function addAction(Request $request){
        $form = $this->createForm(LikeJackerType::class, new LikeJacker());

        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            return $this->render('AppAdminBundle:LikeJacking:add.html.twig',array('form'=>$form->createView()));
        } else {
            $form->handleRequest($request);

            // Si le formulaire est valide
            if ($form->isValid()) {
                $em = $this->container->get("doctrine.orm.default_entity_manager");
                $form = $form->getData();

                $json['submitted'] = true;

                $em->persist($form);
                $em->flush();
                // La redirection
                $json['redirect'] = $this->get('router')->generate('likejacking_gestion');

            } else {

                $json['errors'] = array(
                    'symfony' => FormUtils::getErrorMessages($form)
                );

            }

            return new JsonResponse($json);
        }
    }

    protected function getPhotoUploader()
    {
        return $this->get('app_upload.photo_uploader');
    }

    function getLikeJackRepo(){
        return $this->getDoctrine()->getRepository('AppBdBundle:LikeJacker');
    }

}

<?php

namespace App\AdminBundle\Controller;

use App\AdminBundle\Utils\FormUtils;
use App\BdBundle\Entity\Account;
use App\BdBundle\Entity\Article;
use App\BdBundle\Entity\Page;
use App\BdBundle\Entity\Publi;
use App\BdBundle\Form\AccountType;
use App\BdBundle\Form\PageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

class FacebookAccountController extends Controller
{



    public function indexAction()
    {

        $accounts = $this->getAccountsRepo()->findAll();
        $pages = $this->getPageRepo()->findAll();

        return $this->render('AppAdminBundle:Facebook:account-index.html.twig', array('accounts' => $accounts,'pages'=>$pages));
    }

    public function AddPageAction(Request $request){
        $page = new Page();

        $form = $this->createForm(PageType::class, $page);

        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            return $this->render('AppAdminBundle:Facebook:page-add.html.twig', array('form' => $form->createView()));
        } else {

            $form->handleRequest($request);

            // Si le formulaire est valide
            if ($form->isValid()) {
                $em = $this->container->get("doctrine.orm.default_entity_manager");
                $form = $form->getData();

                $json['submitted'] = true;

                $em->persist($form);
                $em->flush();
                // La redirection
                $json['redirect'] = $this->get('router')->generate('facebook_account');

            } else {

                $json['errors'] = array(
                    'symfony' => FormUtils::getErrorMessages($form)
                );

            }

            return new JsonResponse($json);
        }
    }

    public function EditPageAction(Page $page, Request $request){
        $form = $this->createForm(PageType::class, $page);


        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            return $this->render('AppAdminBundle:Facebook:page-add.html.twig', array('form' => $form->createView()));
        } else {
            $form->handleRequest($request);

            // Si le formulaire est valide
            if ($form->isValid()) {
                $em = $this->container->get("doctrine.orm.default_entity_manager");
                $form = $form->getData();

                $json['submitted'] = true;

                $em->persist($form);
                $em->flush();
                // La redirection
                $json['redirect'] = $this->get('router')->generate('facebook_account');

            } else {

                $json['errors'] = array(
                    'symfony' => FormUtils::getErrorMessages($form)
                );

            }

            return new JsonResponse($json);
        }
    }

    public function editAction(Account $account, Request $request){

        $form = $this->createForm(AccountType::class, $account);


        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            return $this->render('AppAdminBundle:Facebook:account-add.html.twig', array('form' => $form->createView()));
        } else {
            $form->handleRequest($request);

            // Si le formulaire est valide
            if ($form->isValid()) {
                $em = $this->container->get("doctrine.orm.default_entity_manager");
                $form = $form->getData();

                $json['submitted'] = true;

                $em->persist($form);
                $em->flush();
                // La redirection
                $json['redirect'] = $this->get('router')->generate('facebook_account');

            } else {

                $json['errors'] = array(
                    'symfony' => FormUtils::getErrorMessages($form)
                );

            }

            return new JsonResponse($json);
        }
    }


    public function addAction(Request $request)
    {

        $account = new Account();
        $form = $this->createForm(AccountType::class, $account);


        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            return $this->render('AppAdminBundle:Facebook:account-add.html.twig', array('form' => $form->createView()));
        } else {
            $form->handleRequest($request);

            // Si le formulaire est valide
            if ($form->isValid()) {
                $em = $this->container->get("doctrine.orm.default_entity_manager");
                $form = $form->getData();

                $json['submitted'] = true;

                $em->persist($form);
                $em->flush();
                // La redirection
                $json['redirect'] = $this->get('router')->generate('facebook_account');

            } else {

                $json['errors'] = array(
                    'symfony' => FormUtils::getErrorMessages($form)
                );

            }

            return new JsonResponse($json);
        }
    }

    public function deleteAction(Account $account){
        $em = $this->container->get("doctrine.orm.default_entity_manager");

        $em->remove($account);
        $em->flush();

        return $this->redirect($this->get('router')->generate('facebook_account'));
    }

    public function deletePageAction(Page $page){
        $em = $this->container->get("doctrine.orm.default_entity_manager");

        $em->remove($page);
        $em->flush();

        return $this->redirect($this->get('router')->generate('facebook_account'));
    }

    function getPageRepo(){
        return $this->getDoctrine()->getRepository('AppBdBundle:Page');
    }

    function getAccountsRepo()
    {
        return $this->getDoctrine()->getRepository('AppBdBundle:Account');
    }

}

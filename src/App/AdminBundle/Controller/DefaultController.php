<?php

namespace App\AdminBundle\Controller;

use App\BdBundle\Entity\Publi;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\DateTime;

class DefaultController extends Controller
{


    public function indexAction()
    {

        return $this->render('AppAdminBundle:Default:index.html.twig');
    }

    public function activatePubliAction($id)
    {


        return $this->redirect( $this->generateUrl('app_admin_homepage') );

    }

    public function deletePubliAction($id){

        return $this->redirect( $this->generateUrl('app_admin_homepage') );
    }

    public function deleteArticleAction($id){


        return $this->redirect( $this->generateUrl('app_admin_homepage') );
    }
}

<?php

namespace App\AdminBundle\Controller;

use App\AdminBundle\Utils\FormUtils;
use App\BdBundle\Entity\Article;
use App\BdBundle\Entity\Publi;
use App\BdBundle\Entity\Video;
use App\BdBundle\Form\ArticleType;
use App\BdBundle\Form\VideoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Util\FormUtil;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

class VideoController extends Controller
{
    public function addAction(Request $request, $id = null)
    {
        $video = new Video();
        $formAjout = $this->createForm(VideoType::class, $video);

        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            return $this->render('AppAdminBundle:Video:ajout.html.twig', array('form' => $formAjout->createView()));
        } else {
            $formAjout->handleRequest($request);

            // Si le formulaire est valide
            if ($formAjout->isValid()) {
                die(dump('todo : validate form !! '));
            } else {

                $json['errors'] = array(
                    'symfony' => FormUtils::getErrorMessages($formAjout)
                );


            }

            return new JsonResponse($json);

        }
    }


    public function getVideoRepo(){
        return $this->getDoctrine()->getRepository('AppBdBundle:Video');
    }
}

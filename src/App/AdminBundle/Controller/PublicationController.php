<?php

namespace App\AdminBundle\Controller;

use App\AdminBundle\Utils\FormUtils;
use App\BdBundle\Entity\Publi;
use App\BdBundle\Form\PubliType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

class PublicationController extends Controller
{

    public function SendPubliAction(Publi $publi){
        $em = $this->container->get("doctrine.orm.default_entity_manager");
        $facebookPusher = $this->getFacebookPusher();

        $return = $facebookPusher->push($publi);

        if($return == true){
            $publi->setIsSentFacebook(true);
            $em->flush();
        }

        return $this->redirect($this->get('router')->generate('publication_gestion'));
    }



    public function indexAction()
    {
        $publications = $this->getPubliRepo()->findAll();
        return $this->render('AppAdminBundle:Publication:index.html.twig', array('publis'=>$publications));
    }

    public function editAction(Publi $publi, Request $request){

        $form = $this->createForm(PubliType::class, $publi);

        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            return $this->render('AppAdminBundle:Publication:add.html.twig',array('form'=>$form->createView()));
        } else {
            $form->handleRequest($request);

            // Si le formulaire est valide
            if ($form->isValid()) {
                $em = $this->container->get("doctrine.orm.default_entity_manager");
                $form = $form->getData();

                $json['submitted'] = true;

                $em->persist($form);
                $em->flush();
                // La redirection
                $json['redirect'] = $this->get('router')->generate('publication_gestion');

            } else {

                $json['errors'] = array(
                    'symfony' => FormUtils::getErrorMessages($form)
                );

            }

            return new JsonResponse($json);
        }
    }

    public function deleteAction(Publi $publi){
        $em = $this->container->get("doctrine.orm.default_entity_manager");

        $em->remove($publi);
        $em->flush();

        return $this->redirect($this->get('router')->generate('publication_gestion'));
    }

    public function addAction(Request $request){
        $form = $this->createForm(PubliType::class, new Publi());

        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            return $this->render('AppAdminBundle:Publication:add.html.twig',array('form'=>$form->createView()));
        } else {
            $form->handleRequest($request);

            // Si le formulaire est valide
            if ($form->isValid()) {
                $em = $this->container->get("doctrine.orm.default_entity_manager");
                $form = $form->getData();

                $json['submitted'] = true;

                $em->persist($form);
                $em->flush();
                // La redirection
                $json['redirect'] = $this->get('router')->generate('publication_gestion');

            } else {

                $json['errors'] = array(
                    'symfony' => FormUtils::getErrorMessages($form)
                );

            }

            return new JsonResponse($json);
        }

    }

    public function getPubliRepo(){
        return $this->getDoctrine()->getRepository('AppBdBundle:Publi');
    }


    protected function getFacebookPusher()
    {
        return $this->get('app_facebook.facebook_pusher');
    }


}

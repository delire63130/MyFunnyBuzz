<?php

namespace App\AdminBundle\Controller;

use App\BdBundle\Entity\Images;
use App\BdBundle\Entity\Publi;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use App\AppUploadBundle\PhotoUploader;

class MediathequeController extends Controller
{
    public function addAction()
    {
        $imageRepo = $this->getImageRepo();
        $files = $imageRepo->findByDateDesc();

        return $this->render('AppAdminBundle:Mediatheque:images.html.twig', array('files'=>$files));
    }

    public function addFileAction(Request $request){
        $em = $this->container->get("doctrine.orm.default_entity_manager");

        $media = $request->files->get('file');

        $file = $this->getPhotoUploader()->upload($media);

        $image = new Images();
        $image->setDateAjout(new \DateTime());
        $image->setType($file['type']);
        $image->setUrl($file['url']);

        $em->persist($image);
        $em->flush();

        return new JsonResponse(array('success' => true));
    }

    protected function getPhotoUploader()
    {
        return $this->get('app_upload.photo_uploader');
    }

    function getImageRepo(){
        return $this->getDoctrine()->getRepository('AppBdBundle:Images');
    }

}

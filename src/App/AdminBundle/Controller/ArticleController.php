<?php

namespace App\AdminBundle\Controller;

use App\AdminBundle\Utils\FormUtils;
use App\BdBundle\Entity\Article;
use App\BdBundle\Entity\Publi;
use App\BdBundle\Form\ArticleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Util\FormUtil;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

class ArticleController extends Controller
{
    public function addAction(Request $request, $id = null)
    {
        $article = new Article();
        $formAjout = $this->createForm(ArticleType::class, $article);

        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            return $this->render('AppAdminBundle:Article:ajout.html.twig', array('form' => $formAjout->createView()));
        } else {
            $formAjout->handleRequest($request);

            // Si le formulaire est valide
            if ($formAjout->isValid()) {
                $em = $this->container->get("doctrine.orm.default_entity_manager");
                $formAjout = $formAjout->getData();

                $json['submitted'] = true;
                // On enregistre en BDD
                $formAjout->setDate(new \DateTime());

                $em->persist($formAjout);
                $em->flush();
                // La redirection
                $json['redirect'] = $this->get('router')->generate('edit_article');
            } else {

                $json['errors'] = array(
                    'symfony' => FormUtils::getErrorMessages($formAjout)
                );


            }

            return new JsonResponse($json);

        }

    }

    public function editAction(Request $request, $id)
    {
        $article = $this->getArticleRepository()->find($id);
        $formAjout = $this->createForm(ArticleType::class, $article);

        if (!$request->isXmlHttpRequest() || $request->getMethod() !== 'POST') {
            return $this->render('AppAdminBundle:Article:ajout.html.twig', array('form' => $formAjout->createView()));
        } else {
            $formAjout->handleRequest($request);

            // Si le formulaire est valide
            if ($formAjout->isValid()) {
                $em = $this->container->get("doctrine.orm.default_entity_manager");
                $formAjout = $formAjout->getData();

                $json['submitted'] = true;
                // On enregistre en BDD
                $formAjout->setDate(new \DateTime());

                $em->persist($formAjout);
                $em->flush();
                // La redirection
                $json['redirect'] = $this->get('router')->generate('edit_article');
            } else {

                $json['errors'] = array(
                    'symfony' => FormUtils::getErrorMessages($formAjout)
                );


            }

            return new JsonResponse($json);

        }

    }

    public function deleteAction(Article $article){
        $em = $this->container->get("doctrine.orm.default_entity_manager");
        $em->remove($article);
        $em->flush();

        return $this->redirect($this->get('router')->generate('edit_article'));

    }

    public function indexEditAction(){
        $articles = $this->getArticleRepository()->findAll();

        return $this->render('AppAdminBundle:Article:edit.html.twig', array('articles' => $articles));

    }

    public function getArticleRepository(){
        return $this->getDoctrine()->getRepository('AppBdBundle:Article');
    }
}

<?php

namespace App\AdminBundle\Controller;

use App\BdBundle\Entity\Publi;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

class MenuController extends Controller
{
    /**
     * Permet d'afficher le menu latéral gauche.
     *
     * @param Request	$request  La requêtte HTTP.
     *
     * @return Response
     */
    public function menuLeftAction(Request $request)
    {

        return $this->render('AppAdminBundle:Parts:menu-left.html.twig',array('request'=>$request));
    }

    /**
     * Permet d'afficher le menu latéral gauche.
     *
     * @param Request	$request  La requêtte HTTP.
     *
     * @return Response
     */
    public function headerAction(Request $request)
    {

        return $this->render('AppAdminBundle:Parts:header.html.twig', array());
    }


}

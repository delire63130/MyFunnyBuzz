$(function() {

    for (var i in CKEDITOR.instances) {

        CKEDITOR.instances[i].on('change', function() { CKEDITOR.instances[i].updateElement() });

    }

    $that = $('form');
    formHdlr.handle($that, formHdlr.onDcoSuccess);

// On ajoute les boutons de soummission et de RAZ
    formHdlr.addSubmitter($that, $('.submitter'));
    formHdlr.addRazer($that, $('#raz_form'));
});
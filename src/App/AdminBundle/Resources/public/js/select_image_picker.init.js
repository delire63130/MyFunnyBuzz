$("#app_bdbundle_article_imageCover").imagepicker({
    hide_select : true,
    show_label  : false
});

$("#app_bdbundle_article_imageEditor").imagepicker({
    hide_select : true,
    show_label  : false
});

$("#app_bdbundle_article_imageEditor").change(function(){

    valueUrl = $(this).val().replace('180x180','original');
    var image_tag="<img src='"+valueUrl+"'>";


    CKEDITOR.instances.app_bdbundle_article_content.insertHtml(image_tag);

});


$('#paginateButton').click(function(){
    CKEDITOR.instances.app_bdbundle_article_content.insertHtml('$$$paginate$$$');
});
/**
 * Modal est la classe représentant les popups ou modals.
 *
 * @class
 * @author Adrien MANEN
 *
 * @param {object} options Un objet JSON avec les options de la popup
 */
function Modal(options)
{
    options = $.extend({}, Modal.byDefault, options || {});

    this.$dom = null;
    this.type = options.type;
    this.title = options.title;
    this.content = options.content;
    this.extrasClass = options.extrasClass;
    this.size = options.size;

    this.actions = options.actions;
    this.onClose = options.onClose;
    this.onShow = options.onShow;
    this.onContentAdded = options.onContentAdded;

    // On créé le DOM
    this.makeHtml();
}

Modal.SIZE_LARGE = "modal-lg";
Modal.SIZE_NORMAL = "";
Modal.SIZE_SMALL = "modal-sm";

Modal.TYPE_FAILURE = Modal.TYPE_ERROR = {
    "class" : "failure",
    "glyph" : '<i class="fa fa-times-circle"></i>'
};
Modal.TYPE_WARNING = Modal.TYPE_ATTENTION = {
    "class" : "warning",
    "glyph" : '<i class="fa fa-exclamation-triangle"></i>'
};
Modal.TYPE_NOTE = Modal.TYPE_INFO = {
    "class" : "note",
    "glyph" : '<i class="fa fa-comment-o"></i>'
};
Modal.TYPE_SUCCESS = Modal.TYPE_OK = {
    "class" : "success",
    "glyph" : '<i class="fa fa-check-circle"></i>'
};
Modal.TYPE_PROMPT = Modal.TYPE_ASK = {
    "class" : "ask",
    "glyph" : '<i class="fa fa-question-circle"></i>'
};
Modal.TYPE_WAIT = Modal.TYPE_WORKING = {
    "class" : "",
    "glyph" : '<i class="fa fa-cog fa-spin"></i>'
};
Modal.TYPE_WAIT = Modal.TYPE_SEARCH = {
    "class" : "",
    "glyph" : '<i class="fa fa-search"></i>'
};
Modal.TYPE_NORMAL = Modal.TYPE_NONE = {
    "class" : "",
    "glyph" : ""
};
Modal.TYPE_UPLOAD = Modal.TYPE_UPLOAD = {
    "class" : "",
    "glyph" : '<i class="fa fa-upload"></i>'
};

Modal.byDefault = {
    type : Modal.TYPE_NORMAL,
    title : "Notification",
    content : "",
    extrasClass : "",
    size : Modal.SIZE_NORMAL,
    unclosable : false,
    actions : {
        "OK" : {
            "onClick" : function() {
                this.hide();
            }
        }
    },
    onShow : function() {},
    onContentAdded : function() {},
    onClose : function(event) {
        this.destroy();
    }
};

Modal.prototype.hide = function() {
    this.$dom.modal('hide');
    return this;
};

Modal.prototype.show = function() {
    this.$dom.modal('show');
    return this;
};

Modal.prototype.toggle = function() {
    this.$dom.modal('toggle');
    return this;
};

Modal.prototype.destroy = function() {
    this.$dom.off().remove();
    return this;
};

Modal.prototype.toggleClass = function(cls) {
    this.$dom.find('.modal-body').toggleClass(cls);
    return this;
};

Modal.prototype.setTitle = function(title) {
    var $title = this.$dom.find('.modal-title');

    this.title = title;

    $title.html(this.type.glyph + this.title);

    return this;
};

Modal.prototype.setContent = function(html) {
    var $content = this.$dom.find('.modal-body');

    this.content = html;

    $content.html(html);

    return this;
};

Modal.prototype.makeHtml = function() {
    var me = this,
        lib, func, $button,
        html = {
            "div" : {
                "@class" : "modal fade "+ this.type["class"],
                "@tabindex" : -1,
                "@role" : "dialog",
                "@aria-labelledby" : "myModalLabel",
                "@aria-hidden" : "true",
                "div" : {
                    "@class" : "modal-dialog "+ this.size,
                    "div" : {
                        "@class" : "modal-content",
                        "div" : [{
                            "@class" : "modal-header",
                            "button" : {
                                "@type" : "button",
                                "@class" : "close",
                                "@data-dismiss" : "modal",
                                "span" : [{
                                    "@aria-hidden" : "true",
                                    "#text" : "&times;"
                                },{
                                    "@class" : "sr-only",
                                    "#text" : "Fermer"
                                }]
                            },
                            "h4" : {
                                "@class" : "modal-title",
                                "@id" : "myModalLabel",
                                "#text" : this.type.glyph + this.title
                            }
                        },{
                            "@class" : "modal-body "+ this.extrasClass
                        },{
                            "@class" : "modal-footer"
                        }]
                    }
                }
            }
        };

    // On transforme le JSON en HTML
    html = json2xml(html, "");

    // On fait l'objet DOM via JQuery
    this.$dom = $(html);
    this.$dom.on('hidden.bs.modal', me.onClose.bind(this));
    this.$dom.on('shown.bs.modal', me.onShow.bind(this));

    // Si on a pas de contenu
    if(this.content == "" || !this.content)
    {
        this.$dom.find(".modal-header").css("border", "none");
        this.$dom.find(".modal-body").remove();
    }
    else
    {
        this.$dom.find('.modal-body').append(this.content);
    }

    this.onContentAdded.call(this);

    // On créé les actions
    for(lib in this.actions)
    {
        html = {
            "button" : {
                "@type" : "button",
                "@class" : "btn btn-default",
                "#text" : lib
            }
        };

        html = json2xml(html, "");
        func = this.actions[lib].onClick;
        extrasClass = this.actions[lib].extrasClass;

        if(!func) {
            func = Modal.byDefault.actions.OK.onClick;
        }

        // On créé le DOM du bouton en lui associant sa callback
        $button = $(html).on('click', func.bind(this));

        if(extrasClass) {
            $button.addClass(extrasClass);
        }

        // On ajoute le bouton à la liste des boutons de la modal
        this.$dom.find(".modal-footer").append($button);
    }

    this.$dom.appendTo('body');
};

Modal.confirm = function(title, html, onConfirm, onInfirm, onShow) {
    return new Modal({
        type : Modal.TYPE_PROMPT,
        title : title ? title : "Veuillez confirmer",
        size : Modal.SIZE_LARGE,
        extrasClass : "text-justify",
        content : html,
        onShow : onShow,
        onClose : function(event) {
            if(typeof onClose == 'function') onInfirm.apply(this);
            this.destroy();
        },
        actions : {
            "<strong>OUI</strong>, confirmer" : {
                extrasClass : "apply",
                onClick : function() {
                    if(typeof onConfirm == 'function') onConfirm.apply(this);
                    this.hide();
                }
            },
            "<strong>NON</strong>, annuler" : {
                extrasClass : "cancel",
                onClick : function() {
                    if(typeof onInfirm == 'function') onInfirm.apply(this);
                    this.hide();
                }
            }
        }
    }).show();
};

Modal.error = function(title, html, onClose, onShow) {
    return new Modal({
        type : Modal.TYPE_FAILURE,
        title : title ? title : "Erreur",
        size : Modal.SIZE_LARGE,
        extrasClass : "text-justify",
        content : html,
        onShow : onShow,
        onClose : function(event) {
            if(typeof onClose == 'function') onClose.apply(this);
            this.destroy();
        },
        actions : {
            "OK" : {
                extrasClass : "text-uppercase",
                onClick : function() {
                    if(typeof onClose == 'function') onClose.apply(this);
                    this.hide();
                }
            }
        }
    }).show();
};

Modal.success = function(title, html, onClose, onShow) {
    return new Modal({
        type : Modal.TYPE_SUCCESS,
        title : title ? title : "Succès",
        size : Modal.SIZE_LARGE,
        extrasClass : "text-justify",
        content : html,
        onShow : onShow,
        onClose : function(event) {
            if(typeof onClose == 'function') onClose.apply(this);
            this.destroy();
        },
        actions : {
            "OK" : {
                extrasClass : "text-uppercase",
                onClick : function() {
                    if(typeof onClose == 'function') onClose.apply(this);
                    this.hide();
                }
            }
        }
    }).show();
};

Modal.info = function(title, html, onClose, onShow) {
    return new Modal({
        type : Modal.TYPE_NOTE,
        title : title ? title : "Information",
        size : Modal.SIZE_LARGE,
        extrasClass : "text-justify",
        content : html,
        onShow : onShow,
        onClose : function(event) {
            if(typeof onClose == 'function') onClose.apply(this);
            this.destroy();
        },
        actions : {
            "OK" : {
                extrasClass : "text-uppercase",
                onClick : function() {
                    if(typeof onClose == 'function') onClose.apply(this);
                    this.hide();
                }
            }
        }
    }).show();
};

Modal.working = function(title, html, onClose, onShow) {
    return new Modal({
        type : Modal.TYPE_WORKING,
        title : title ? title : "Traitement en cours...",
        size : Modal.SIZE_LARGE,
        extrasClass : "text-justify",
        content : html ? html : '<div class="text-center"><i class="fa fa-cog fa-spin fa-5x"></i></div>',
        onShow : onShow,
        unclosable : true,
        onClose : function(event) {
            if(typeof onClose == 'function') onClose.apply(this);
            this.destroy();
        },
        actions : {}
    }).show();
};
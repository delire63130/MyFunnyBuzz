/**
 * DcoHandler est la classe gérant les dictionnaires et leurs champs.
 *
 * @class
 * @author Adrien MANEN
 */
function FormHandler()
{
    /**
     * La liste des formulaires gérés.
     * Format : formName => $form
     *
     * @member {object<string, jQuery>}
     */
    this.forms = {};

    /**
     * La liste des boutons permettant la soumission.
     * Format : formName => $button
     *
     * @member {object<string, jQuery>}
     */
    this.submitters = {};

    /**
     * La liste des boutons permettant la RAZ.
     * Format : formName => $button
     *
     * @member {object<string, jQuery>}
     */
    this.razers = {};
}

/**
 * L'instance de FormHandler, accessible dans le contexte global.
 *
 * @global
 * @type {FormHandler}
 */
var formHdlr;

/**
 * Ajoute un élément de type "submit" à un formulaire.
 *
 * @function
 * @memberof FormHandler
 * @this {FormHandler}
 *
 * @param {mixed} $form Le nom du formulaire à soumettre, le DOM du formulaire, ou un middleware renvoyant le formulaire
 * @param {jQuery} $dom L'élément DOM qui va servir de bouton de soumission du formulaire
 */
FormHandler.prototype.addSubmitter = function($form, $dom)
{
    if(typeof $form == "string")
    {
        $form = this.forms[$form];

        if(!$form)
        {
            var error = 'Impossible d\'ajouter un bouton de soumission pour le formulaire "'+ $form +'"';
            error += '. As-tu fais un formHdlr.handle($("form[name=\''+ $form +'\']")) avant ?';

            console.error(error);
            return;
        }
    }

    if(typeof $form != "function")
    {
        this.submitters[$form.attr('name')] = $dom;
    }

    $dom.on('click', function(event)
    {
        // On stoppe la propagation de l'évènement
        event.preventDefault();
        event.stopPropagation();

        var $formulaire = (typeof $form == "function") ? $form() : $form;

        if($formulaire.data('submitted') == false)
        {
            $formulaire.data('submitted', true).submit();
        }
    });
};

/**
 * Ajoute un élément de type "reset" à un formulaire.
 *
 * @function
 * @memberof FormHandler
 * @this {FormHandler}
 *
 * @param {string} form Le nom du formulaire à soumettre, ou le DOM du formulaire directement
 * @param {jQuery} $dom L'élément DOM qui va servir de bouton de soumission du formulaire
 */
FormHandler.prototype.addRazer = function($form, $dom)
{
    var that = this;

    if(typeof $form == "string")
    {
        $form = this.forms[$form];

        if(!$form)
        {
            var error = 'Impossible d\'ajouter un bouton de RAZ pour le formulaire "'+ $form +'"';
            error += '. As-tu fais un formHdlr.handle($("form[name=\''+ $form +'\']")) avant ?';

            console.error(error);
            return;
        }
    }

    if(typeof $form != "function")
    {
        this.razers[$form.attr('name')] = $dom;
    }

    $dom.on('click', function()
    {
        // On stoppe la propagation de l'évènement
        event.preventDefault();

        // On RAZ le formulaire
        if(typeof $form == "function")
        {
            that.raz($form());
        }
        else
        {
            that.raz($form);
        }
    });
};

FormHandler.prototype.handleXhrError = function(jqXHR, textStatus, errorThrown)
{
    var html = "<p>La soumission du formulaire s'est terminée avec une erreur :</p>";

    html += "<ul>";
    html += "<li>Code HTTP : "+ jqXHR.status +"</li>";
    html += "<li>Statut : "+ textStatus +"</li>";

    if(errorThrown.length > 0)
    {
        html += "<li>Message : "+ errorThrown +"</li>";
    }


    html += "</ul>";

    Modal.error("Erreur de l'application", html, function()
    {
        //document.location.reload(true);
    });
};

/**
 * Permet de gérer le formulaire donné via le gestionnaire.
 *
 * @function
 * @memberof FormHandler
 * @this {FormHandler}
 *
 * @param {jQuery} $form Le formulaire à gérer
 * @param {function} onSuccess La fonction de rappel en cas de succès
 * @param {function} onError La fonction de rappel en cas d'erreur
 */
FormHandler.prototype.handle = function($form, onSuccess, onError)
{
    var me = this;

    if(typeof $form == "string")
    {
        this.forms[$form] = $("form[name='"+ $form +"']");
        $form = this.forms[$form];
    }
    else
    {
        this.forms[$form.attr('name')] = $form;
    }

    // On attache un validateur
    var vvv = Validator.attachTo($form);

    // Lors de la soumission du formulaire
    $form.data('submitted', false).on('submit', function(event)
    {
        // On stoppe la propagation de l'évènement
        event.preventDefault();
        event.stopPropagation();

        /*
         // On vérif si le formulaire est valide
         if(!vvv.isValid())
         {
         // Pour un blocage de la validation
         var $err = vvv.getFirstInvalid();
         vvv.gotoInput($err);
         return;
         }
         */

        // On affiche une modal temporaire
        var mdl = Modal.working('Veuillez patienter...');

        // On envoie le formulaire au serveur
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: new FormData( this ),
            //data: $(this).serialize(),
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'json',
            context: me,
            timeout: 120000,
            success: function(json)
            {
                mdl.hide();

                // console.log(json);

                if(typeof onSuccess == "function")
                {
                    onSuccess(json, vvv, $form);
                }
            },
            error : function(jqXHR, textStatus, errorThrown)
            {
                mdl.hide();

                $form.data('submitted', false);

                if(typeof onError == "function")
                {
                    onError(jqXHR.status, textStatus, errorThrown);
                }
                else
                {
                    this.handleXhrError(jqXHR, textStatus, errorThrown);
                }
            }
        });

        return false;
    });
};

/**
 * Permet de réinitialiser un formulaire
 *
 * @function
 * @memberof FormHandler
 * @this {FormHandler}
 *
 * @param {jQuery} $form Le formulaire à réinitialiser
 */
FormHandler.prototype.raz = function($form)
{
    if(typeof $form == "string")
    {
        $form = $("form[name='"+ $form +"']");
    }

    var html = "";

    html += "<p>Toutes les données saisies dans le formulaire vont être effacées.</p>";
    html += "<p>Vous confirmez ?</p>";

    var mdl = Modal.confirm("Demande de confirmation", html, function()
    {
        $form.data('submitted', false)[0].reset();
        Validator.update( $form );
    });
};

/**
 * Parse la réponse envoyé par le service Symfony DcoUtils.
 *
 * @function
 * @memberof FormHandler
 * @this {FormHandler}
 *
 * @param {object} json Le json de la réponse
 * @param {Validator} vvv Le validateur de formulaires
 */
FormHandler.prototype.onDcoSuccess = function(json, vvv, $form)
{
    var html = "", error;

    // Si le formulaire est valide
    if(json.submitted == true)
    {
        if(json.message)
        {
            html += "<h3>"+ json.message +"</h3>";
        }
        else
        {
            html += "<h3>Le formulaire a été traité avec succès</h3>";
        }

        mdl = Modal.success("Succès", html, function()
        {
            if(json.redirect) {
                document.location.href = json.redirect;
            }
        });
    }
    else
    {
        $form.data('submitted', false);

        if(!json.errors)
        {
            return;
        }

        html += "<p>Le formulaire comporte une ou plusieurs erreurs et n'a donc pas pu être validé.</p>";
        html += "<p>Voici la liste des erreurs commises :</p>";
        html += "<ul class='error-list'>";

        // Les erreurs sur les dictionnaires
        for(var dcoName in json.errors.dictionnaires)
        {
            error = json.errors.dictionnaires[dcoName];

            html += "<li data-input=\"[name='"+ dcoName +"']\"><i class='fa fa-link'></i><span>"+ error +"</span></li>";
        }

        // Les erreurs symfony
        if(json.errors.symfony && Array.isArray(json.errors.symfony))
        {
            for(idx in json.errors.symfony)
            {
                error = json.errors.symfony[idx];

                html += "<li>";
                html += "<i class='fa fa-exclamation-circle'></i>";
                html += "<span>"+ error +"</span>";
                html += "</li>";
            }
        }
        else if(json.errors.symfony && typeof json.errors.symfony === 'object')
        {
            // Les erreurs sur les champs symfony
            for(var fieldName in json.errors.symfony)
            {
                error = json.errors.symfony[fieldName];

                // Si on a plusieurs erreurs sur un champ
                if(typeof error == 'object')
                {
                    for(var idx in error)
                    {
                        html += "<li data-input='#"+ fieldName +"'>";
                        html += "<i class='fa fa-link'></i>";
                        html += "<span>"+ error[idx] +"</span>";
                        html += "</li>";
                    }
                }

                // Si on a qu'une erreur
                else
                {
                    html += "<li data-input='#"+ fieldName +"'>";
                    html += "<i class='fa fa-link'></i>";
                    html += "<span>"+ error +"</span>";
                    html += "</li>";
                }
            }
        }

        // Si une raison a été précisée
        if(json.errors.reason)
        {
            html += "<li>";
            html += "<i class='fa fa-exclamation-circle'></i>";
            html += "<span>"+ json.errors.reason +"</span>";
            html += "</li>";
        }

        html += "</ul>";

        // On créé la modal d'erreur
        mdl = Modal.error("Erreur de saisie", html, null, function()
        {
            var me = this;

            this.$dom.find('ul.error-list li > i, ul.error-list li > span').on('click', function()
            {
                var $that = $(this),
                    field = $that.parent().data('input');

                if(field)
                {
                    me.hide();

                    vvv.gotoInput( $(field) );
                }
            });
        });
    }
};

// Lorsque le DOM est chargé
$(function()
{
    // On créé le gestionnaire de formulaires si celui-ci n'existe pas.
    if(!formHdlr)
    {
        formHdlr = new FormHandler();
    }

});
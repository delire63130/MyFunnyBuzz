/**
 * Classe Validator.
 *
 * Utilisé pour valider un formulaire de la BPU. Valide aussi les dictionnaires.
 * Mettre un élément formID-count pour afficher le compteur d'erreurs.
 *
 * @param $form {jQuery} Le formulaire à valider.
 * @constructor
 */
function Validator($form)
{
    this.$dom = {
        $form : $form,
        $count : null
    };

    this.inputs = {};
    this.errorCount = 0;
}

/**
 * La liste des objets Validator attachés.
 *
 * @type {Array}
 */
Validator.instances = [];

Validator.masks = {
    'any' : {
        regex : /^[\s\S]*$/,
        targets : '.mask-any',
        format : function(value) {return value}
    },
    'empty': {
        regex : /^[\s]*$/,
        targets : '.mask-empty',
        format : function(value) {return ""}
    },
    'date': {
        regex : /^[0-3]\d(\/|-)(0[1-9]|1[0-2])(\/|-)\d{4}$/,
        targets : '.mask-date',
        format : function(value) {return value.replace(/\//g, '-')}
    },
    'date-no-year': {
        regex : /^[0-3]\d(\/|-)(0[1-9]|1[0-2])$/,
        targets : '.mask-date-no-year',
        format : function(value) {return value.replace(/\//g, '-')}
    },
    'year': {
        regex : /^\d{4}$/,
        targets : '.mask-year',
        format : function(value) {return value}
    },
    'name': {
        regex : /^([a-zâàäçéèêëêôöîïù][a-z0-9âàäçéèêëêôöîïù'\-]{2,40})?$/i,
        targets : '.mask-name',
        format : function(value) {return value}
    },
    'name-spaces': {
        regex : /^([a-zâàäçéèêëêôöîïù](\ ?[a-z0-9âàäçéèêëêôöîïù'\-]+)+)?$/i,
        targets : '.mask-name-spaces',
        format : function(value) {return value}
    },
    'mail': {
        regex : /^([-0-9a-z.+_]+@[-0-9a-z.+_]+\.[a-z]{2,4})?$/i,
        targets : '.mask-mail',
        format : function(value) {return value}
    },
    'tel' : {
        regex : /^((\d{2}([ ]|[.]|[-])?){5}|\d{1}([ ]|[.]|[-])?(\d{3}([ ]|[.]|[-])?){3}|[+]\d{2}([ ]|[.]|[-])?\d{1}([ ]|[.]|[-])?(\d{2}([ ]|[.]|[-])?){4})?$/i,
        targets : '.mask-tel',
        format : function(value) {return value.replace(/[\s\.-]/g, '')}
    },
    'tel-multi' : {
        regex : /\+\d{2} \(0\)\d( \d{2}){4}/i,
        targets : '.mask-tel-multi',
        format : function(value) {return value}
    },
    'codePostal' : {
        regex : /^(\d{2}\ ?\d{3})?$/,
        targets : '.mask-code-postal',
        format : function(value) {return value.replace(/\s/g, '')}
    },
    'amount' : {
        regex : /^(-?\d{1,3}\ ?(\d{3}\ ?)*((\.|,)(\d{1,3}\ ?)+)?)?$/,
        targets : '.mask-amount',
        format : function(value) {return value.replace(/\,/g, '.').replace(/\s/, '')}
    },
    'quantity' : {
        regex : /^\d{1,3}\ ?(\d{3}\ ?)*$/,
        targets : '.mask-quantity',
        format : function(value) {return value.replace(/\s/, '')}
    },
    'porte' : {
        regex : /^[\d]{1,4}[a-z]{0,3}$/i,
        targets : '.mask-porte',
        format : function(value) {return value.replace(/\s/, '')}
    },
    'percent' : {
        regex : function(value)
        {
            var result = false;

            // Si c'est un chiffre
            if(/^[0-9\.]+$/.test(value))
            {
                if(value >= 0 && value <= 100)
                {
                    result = true;
                }
            }

            return result;
        },
        targets : '.mask-percent',
        format : function(value) {return value}
    },
    'hex' : {
        regex : /^\#[a-f0-9]{3}([a-f0-9]{3})?$/i,
        targets : '.mask-hex',
        format : function(value) {return value}
    },
    'positiveInteger' : {
        regex : /^\d+$/,
        targets : '.mask-positiveInteger',
        format : function(value) {return value.replace(/\s/, '')}
    },
    'correctInteger' : {
        regex : /^\d{3,}$/,
        targets : '.mask-correctInteger',
        format : function(value) {return value.replace(/\s/, '')}
    },
    'positiveFloat' : {
        regex : /^(\d*[.,])?\d+$/,
        targets : '.mask-positiveFloat',
        format : function(value) {return value.replace(/\s/, '')}
    }
};

Validator.state = {
    invalid : 0,
    valid : 1,
    none : 2
};

Validator.attachTo = function($formOrInput)
{
    var instance = Validator.getInstanceOf($formOrInput),
        isInput = $formOrInput.is('input, textarea, select, .selectpicker'),
        isForm = $formOrInput.is('form'),
        id = $formOrInput.attr('id');

    if(isForm)
    {
        var validator = (new Validator($formOrInput)).init();

        Validator.instances.push(validator);
    }
    else
    {
        if(!instance)
        {
            instance = Validator.getInstanceOf( $formOrInput.parentsUntil('form').last().parent() );
        }

        // On ajoute l'input à la liste avec ses masks
        instance.handleInput($formOrInput, instance.getMaskFor($formOrInput));

        // On valid le nouvel input
        instance.validInput(instance.inputs[id].$dom);
    }

    return validator;
};

Validator.detach = function($formOrInput)
{
    var instance = Validator.getInstanceOf($formOrInput),
        isInput = $formOrInput.is('input, textarea, select, .selectpicker'),
        isForm = $formOrInput.is('form'),
        id = $formOrInput.attr('id');

    // Si on a une instance
    if(instance)
    {
        // Si c'est un formulaire
        if(isForm)
        {
            for(var idx in instance.inputs)
            {
                instance.off( instance.inputs[idx].$dom );
            }
        }
        // Si c'est un champ
        else if(isInput)
        {
            instance.off( instance.inputs[id].$dom );
        }
    }

    // Si on a pas d'instance, on recherche les éléments susceptible d'être détachés
    else
    {
        $formOrInput.find('input, textarea, select').each(function()
        {
            Validator.detach( $(this) );
        });
    }
};

Validator.update = function($form)
{
    if($form)
    {
        var vldtr = Validator.getInstanceOf($form);

        if(vldtr)
        {
            vldtr.update();
        }
    }
    else
    {
        for(var idx in Validator.instances)
        {
            Validator.instances[idx].update();
        }
    }
};

Validator.generateUUID = function()
{
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c)
    {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
};



Validator.onKeyUp = function(event)
{
    var $that = $(this),
        vldtr = Validator.getInstanceOf($that);

    vldtr.validInput($that);
};

Validator.onChange = function(event)
{
    var $that = $(this),
        vldtr = Validator.getInstanceOf($that);

    vldtr.validInput($that);
};

Validator.getInstanceOf = function($formOrInput)
{
    var instance = null,
        isInput = $formOrInput.is('input, textarea, select'),
        isForm = $formOrInput.is('form'),
        id = $formOrInput.attr('id'),
        inst, idx;

    for(idx in Validator.instances)
    {
        inst = Validator.instances[idx];

        if(
            (isInput && inst.inputs[id])
            || (isForm && inst.$dom.$form.is($formOrInput)))
        {
            instance = inst;
            break;
        }
    }

    return instance;
};

Validator.validate = function($inputOrForm)
{
    var vldtr = Validator.getInstanceOf($inputOrForm),
        isForm = $inputOrForm.is('form'),
        isInput = $inputOrForm.is('input, textarea, select');

    if(vldtr)
    {
        if(isForm)
        {
            vldtr.validForm();
        }
        else if(isInput)
        {
            vldtr.validInput($inputOrForm);
        }
    }
};

Validator.isValid = function($formOrInput)
{
    var vldtr = Validator.getInstanceOf($formOrInput),
        isForm = $formOrInput.is('form'),
        isInput = $formOrInput.is('input, textarea, select'),
        isVal = false;

    if(vldtr)
    {
        if(isForm)
        {
            isVal = vldtr.isValid();
        }
        else if(isInput)
        {
            isVal = vldtr.isValid($formOrInput);
        }
    }

    return isVal;
};

Validator.prototype.init = function()
{
    var me = this,
        formID = this.$dom.$form.attr('id') || this.$dom.$form.attr('name');

    this.$dom.$form
        .find('input, textarea, select')
        .not('[type="hidden"]')
        .each(function()
        {
            if( $(this).parent().hasClass('bs-searchbox') ) return;

            me.check( $(this) );
        });

    if(formID)
    {
        //this.$dom.$count = $('#'+ formID +'-count');
        this.$dom.$count = $('#validator-state');

        // Si le compteur n'a pas déjà été initialisé
        if(!this.$dom.$count.hasClass('tooltipstered'))
        {


            this.$dom.$count.on('click', function(event)
            {
                event.preventDefault();
                event.stopPropagation();

                // On récupère le premier champ invalide
                var $invalidInput = me.getFirstInvalid();

                if($invalidInput)
                {
                    // On positionne l'utilisateur sur ce champ
                    me.gotoInput($invalidInput);
                }
            });
        }
    }

    // On valide le formulaire
    this.validForm();

    return this;
};

Validator.prototype.check = function($input)
{
    var id = $input.attr('id'),
        data = this.inputs[id];

    if(!data)
    {

            this.handleInput($input, this.getMaskFor($input));

    }
};

Validator.prototype.update = function()
{
    var id, data;

    /*
     Très gourmand en ressources
     // On vire les instances des champs qui n'existent plus
     for(id in this.inputs)
     {
     data = this.inputs[id];
     if( $(data.$dom.selector).size() == 0 )
     {
     this.off(data.$dom);
     delete this.inputs[id];
     }
     }
     */

    // On remet
    this.init();
};

Validator.prototype.off = function($input)
{
    var id = $input.attr('id');

    if(id && this.inputs[id])
    {
        // On efface les évènements
        $input.off('keyup', Validator.onKeyUp);
        $input.off('change', Validator.onChange);

        // On supprime le css et les données spécifiques
        $input
            .removeData('valid')
            .removeClass('is-invalid')
            .removeClass('is-valid');

        delete this.inputs[id];
    }
};

Validator.prototype.getMaskFor = function($input)
{
    var mask = null,
        name;

    for(name in Validator.masks)
    {
        if($input.is(Validator.masks[name].targets))
        {
            mask = Validator.masks[name];
            break;
        }
    }

    return mask;
};

Validator.prototype.test = function($input, mask)
{
    var val = $input.val().trim();

    return mask.regex.test(val);
};

Validator.prototype.gotoInput = function($input, yOffsetAdd)
{
    var id = $input.attr('id'),
        data = this.inputs[id],
        visible = $input.is(':visible');

    // Si le champ est caché mais qu'il a un lien
    if(data && data.$tab && !visible)
    {
        data.$tab.click();
    }

    // On regarde si on peut scroller
    var $scroller = $input.parentsUntil('.hasNiceScroll').last().parent(),
        ns = $scroller.getNiceScroll();

    // On scroll jusqu'au champ en question
    if(ns)
    {
        $scroller.scrollTop(0);

        // On défini la marge pour avoir un truc plus présentable
        yOffsetAdd = yOffsetAdd ? yOffsetAdd : 15;

        var start = $scroller.offset().top,
            y = $input.offset().top - start - yOffsetAdd;

        $scroller.scrollTop(y);
    }

    if($input.is('input[type="text"], textarea'))
    {
        // On donne le focus au champ en question
        $input.get(0).select();
        $input.focus();
    }
};

Validator.prototype.getFirstInvalid = function()
{
    var id, $input, data, valid;

    for(id in this.inputs)
    {
        data = this.inputs[id];
        $input = data.$dom;

        valid = $input.data('valid');

        if(valid == Validator.state.invalid)
        {
            break;
        }
    }

    return $input;
};

Validator.prototype.isValid = function($input)
{
    var isValid = true,
        state, id;

    if($input)
    {
        id = $input.attr('id');
        var data = this.inputs[id];

        if(data)
        {
            state = $input.data('valid');

            if(state == Validator.state.invalid)
            {
                isValid = false;
            }
        }
    }
    else
    {
        for(id in this.inputs)
        {
            isValid = this.isValid(this.inputs[id].$dom);

            if(isValid == false)
            {
                break;
            }
        }
    }

    return isValid;
};

Validator.prototype.isText = function($input)
{
    return $input.is('textarea, [type="text"], [type="file"], [type="password"], [type="email"], [type="number"], [type="url"], [type="tel"]');
};

Validator.prototype.isChoice = function($input)
{
    return $input.is('select, [type="radio"], [type="checkbox"]');
};

Validator.prototype.setInvalid = function($input)
{
    var id = $input.attr('id'),
        data = this.inputs[id];

    if((data.isText || data.isSelect) && $input.is('[type="file"]') === false)
    {
        $input.data('valid', Validator.state.invalid);

        // On fait une maj des selectpicker
        if($input.hasClass('selectpicker') && $input.data('selectpicker'))
        {
            $input.selectpicker('setStyle', 'is-valid', 'remove');
            $input.selectpicker('setStyle', 'is-invalid', 'add');
        }
        else
        {
            $input
                .removeClass('is-valid')
                .addClass('is-invalid');
        }
    }
    else
    {
        data.$group.each(function()
        {
            var $that = $(this);

            $that
                .data('valid', Validator.state.invalid)
                .parent().parent()
                .removeClass('zone-is-valid')
                .addClass('zone-is-invalid');
        });
    }
};

Validator.prototype.setValid = function($input)
{
    var id = $input.attr('id'),
        data = this.inputs[id];

    if((data.isText || data.isSelect) && $input.is('[type="file"]') === false)
    {
        $input.data('valid', Validator.state.valid);

        // On fait une maj des selectpicker
        if($input.hasClass('selectpicker'))
        {
            $input.selectpicker('setStyle', 'is-invalid', 'remove');
            $input.selectpicker('setStyle', 'is-valid', 'add');
        }
        else
        {
            $input
                .removeClass('is-invalid')
                .addClass('is-valid');
        }
    }
    else
    {
        data.$group.each(function()
        {
            var $that = $(this);

            $that
                .data('valid', Validator.state.valid)
                .parent().parent()
                .removeClass('zone-is-invalid')
                .addClass('zone-is-valid');
        });
    }
};

Validator.prototype.reset = function($input)
{
    var id = $input.attr('id'),
        data = this.inputs[id];

    if(data.isText || data.isSelect)
    {
        $input.data('valid', Validator.state.none);

        // On fait une maj des selectpicker
        if($input.hasClass('selectpicker'))
        {
            $input.selectpicker('setStyle', 'is-invalid', 'remove');
            $input.selectpicker('setStyle', 'is-valid', 'remove');
        }
        else
        {
            $input
                .removeClass('is-valid')
                .removeClass('is-invalid');
        }
    }
    else
    {
        data.$group.each(function()
        {
            var $that = $(this);

            $that
                .data('valid', Validator.state.none)
                .parent().parent()
                .removeClass('zone-is-invalid')
                .removeClass('zone-is-valid');
        });
    }
};

Validator.prototype.whichTab = function($input)
{
    var $tabPane = $input.parentsUntil('.tab-pane').last().parent(),
        $link = null;

    // S'il appartient à un widget
    if($tabPane.size() > 0 && $tabPane.get(0).tagName)
    {
        var id = $tabPane.attr('id'),
            $widget = $tabPane.parentsUntil('.widget').last().parent(),
            $head;

        if($widget.parent().is('.wizard'))
        {
            $head = $widget.parent().find('.wizard-head');
        }
        else
        {
            $head = $widget.find('.widget-head');
        }

        $link = $head.find('a[href="#'+ id +'"]');
    }

    return ($link && $link.size() > 0) ? $link : null;
};

Validator.prototype.handleDkyInput = function($input, dco)
{
    var id = $input.attr('id');

    var mask = {
        regex : null,
        format : function(value) {return value}
    };

    if(!id) return false;

    if(dco.isTable)
    {
        var first = Object.keys(dco.dicokeys)[0];
        mask.regex = dco.dicokeys[first].mask;
    }
    else
    {
        var dky = dcoHdlr.getDkyByDomId(id);
        mask.regex = dky.mask;
    }

    if(!mask || mask.regex == "" || !mask.regex)
    {
        mask.regex = null;
    }

    // On met en cache le dico
    this.inputs[id] = {
        dco : dco
    };

    // On gère l'input
    this.handleInput($input, mask);
};

Validator.prototype.handleInput = function($input, mask)
{
    var me = this,
        id = $input.attr('id') || Validator.generateUUID(),
        isFile = $input.is('[type="file"]');

    if(!this.inputs[id])
    {
        this.inputs[id] = {};
    }

    $input.data('valid', Validator.state.none);

    // On ajoute l'input à la liste
    this.inputs[id].$dom = $input.attr('id', id);
    this.inputs[id].$group = this.getInputGroup($input);
    this.inputs[id].mask = mask;
    this.inputs[id].required = $input.attr('required');
    this.inputs[id].$tab = this.whichTab($input);
    this.inputs[id].isSelect = $input.is('select');
    this.inputs[id].isChoice = this.isChoice($input);
    this.inputs[id].isText = this.isText($input);
    this.inputs[id].need = this.howManyIdNeeds($input);

    // Lorsque l'utilisateur saisi
    if(this.inputs[id].isText && isFile === false)
    {
        $input.on('keyup', Validator.onKeyUp);
    }
    else if(this.inputs[id].isText && isFile === true)
    {
        $input.on('change', Validator.onKeyUp);
    }
    // Lorsque l'utilisateur sélectionne
    else
    {
        $input.on('change', Validator.onChange);
    }
};

Validator.prototype.howManyIdNeeds = function($input)
{
    var parentCls = $input.parent().parent().attr('class'),
        regex = /(^|\s)need-(\d)(\s|$)/,
        nb = 0;

    // Si on "needs" un nombre d'inputs
    if(parentCls && parentCls.match(regex))
    {
        // On choppe un truc du style ["need-1 ", "", "1", " "]
        var m = parentCls.match(regex);

        if(m.length == 4)
        {
            nb = m[2];
        }
    }

    return nb;
};

Validator.prototype.formatForm = function()
{
    var me = this;

    var id;

    for(id in this.inputs)
    {
        this.formatInput(this.inputs[id].$dom);
    }
};

Validator.prototype.formatInput = function($input)
{
    var id = $input.attr('id'),
        data = this.inputs[id],
        formattedVal = $input.val();
    //formattedVal = $input.val().trim();

    if(data && data.mask)
    {
        formattedVal = data.mask.format.call(this, formattedVal);

        $input.val(formattedVal);
    }
};

Validator.prototype.validForm = function()
{
    var id;

    for(id in this.inputs)
    {
        this.validInput(this.inputs[id].$dom);
    }
};

Validator.prototype.getInputGroup = function($input)
{
    var name = $input.attr('name'),
        dataGroup = $input.data('group'),
        $group = $input;

    if(dataGroup)
    {
        $group = this.$dom.$form.find('[data-group="'+ dataGroup +'"]');
    }
    else
    {
        $group = this.$dom.$form.find('[name="'+ name +'"]');
    }

    return $group;
};

Validator.prototype.validInput = function($input)
{
    var id = $input.attr('id'),
        data = this.inputs[id],
        state = $input.data('valid'),
        $group = data.$group,
        //value = $input.val() ? $input.val().trim() : "",
        //	value = $input.val() ? $input.val() : "",
        value = ($input.val() && !($input.val() instanceof Array))  ? $input.val().trim() : "",
        regexState = Validator.state.valid,
        needState = Validator.state.valid,
        requiredState = Validator.state.valid,
        isDisabled = $input.is(':disabled');

    if(!data) return;

    // Si on a un masque
    if(data.mask && data.mask.regex && data.isText)
    {
        var mask = this.inputs[id].mask,
            match = false;

        if(typeof data.mask.regex == "function")
        {
            match = data.mask.regex(value);
        }
        else
        {
            match = this.test($input, mask);
        }

        // Si le champ est vide et qu'il n'est pas requis
        if(this.test($input, Validator.masks.empty))
        {
            regexState = Validator.state.none;
        }

        // Si le champ est invalide
        else if(match === false)
        {
            regexState = Validator.state.invalid;
        }
    }

    // TODO Prendre en charge les select multiples
    if(data.isChoice && $input.is('[type="checkbox"]'))
    {
        if(data.need > 0 && $group.filter(':checked').size() < data.need)
        {
            needState = Validator.state.invalid;
        }
    }

    // Si le champ est requis et qu'il n'est pas déjà invalide
    if(data.required)
    {
        // Si c'est un champ texte ou un select et qu'il est vide
        if((data.isText || data.isSelect) && value.length == 0)
        {
            requiredState = Validator.state.invalid;
        }

        // Si c'est un input de choix (radio ou select) et que rien n'est choisi
        if(data.isChoice && !data.isSelect && $group.is(':checked') == false)
        {
            requiredState = Validator.state.invalid;
        }
    }

    // Si le champ est intouché
    if(
        (
            !data.required
            && (
                (data.isChoice && data.need == 0)
                || (data.isText && (data.mask && regexState == Validator.state.none) || !data.mask)
            )
        )
        || isDisabled
    )
    {
        this.reset($input);
    }

    // Si le champ est valide
    else if(
        regexState == Validator.state.valid
        && requiredState == Validator.state.valid
        && needState == Validator.state.valid
    )
    {
        this.setValid($input);
    }

    // Si le champ est invalide
    else if(
        regexState == Validator.state.invalid
        || requiredState == Validator.state.invalid
        || needState == Validator.state.invalid
    )
    {
        this.setInvalid($input);
    }

    // Sinon on reset le champ
    else if(state != Validator.state.none)
    {
        this.reset($input);
    }

    // On met à jour le compteur d'erreurs
    this.updateCount();
};

Validator.prototype.updateCount = function()
{
    if(!this.$dom.$count || this.$dom.$count.size() == 0) return;

    var html = "",
        title = "";

    this.errorCount = $('.zone-is-invalid, .is-invalid').not('#validator-state').size();

    switch(this.errorCount)
    {
        case 0:
            this.$dom.$count.removeClass('is-invalid').addClass('is-valid');
            html = '<i class="fa fa-check"></i>';
            title += "Le formulaire semble valide";
            break;

        case 1:
            this.$dom.$count.removeClass('is-valid').addClass('is-invalid');
            html = '<i class="fa fa-times"></i>';
            title += "Une erreur sur le formulaire";
            break;

        default:
            this.$dom.$count.removeClass('is-valid').addClass('is-invalid');
            html = '<i class="fa fa-times"></i>';
            title += this.errorCount +" erreurs sur le formulaire";
            break;
    }

    this.$dom.$count.html(html)

};
<?php
/**
 * Fichier du listener UserRepository.
 *
 * @filesource
 */

namespace App\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;
use App\UserBundle\Entity\User as User;

/**
 * Le dépôt CategoryRepository.
 *
 * Il s'agit du dépôt lié aux entités Category.
 *
 * @author Adrien MANEN
 * @category Dépôts
 */
class UserRepository extends EntityRepository
{

}
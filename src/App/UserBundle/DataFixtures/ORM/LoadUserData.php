<?php
// Change the namespace!

namespace App\UserBundle\DataFixtures\Orm;

use App\UserBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData implements FixtureInterface
{
public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('admin-goliath');
        $user->setEmail("rmycte497@yahoo.fr");
        $user->setPlainPassword('Hothe1957');
        $user->setEnabled("1");
        $user->setLocked("0");
        $user->addRole("ROLE_ADMIN");
        $manager->persist($user);
        $manager->flush();
    }
}
?>
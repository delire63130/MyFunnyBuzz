<?php

namespace App\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MenuController extends Controller
{
    public function MainMenuAction(Request $request)
    {
        $route = $request->attributes->get('_route');
        return $this->render(':parts:menu.html.twig', array('rte'=>$route));
    }

    public function HeaderAction(){

        $trendingArticle = $this->getArticleRepo()->findMoreView(20);
        return $this->render(':parts:header.html.twig', array('trendingArticle'=>$trendingArticle));
    }

    public function footerAction(){


        return $this->render(':parts:footer.html.twig', array());

    }

    function getArticleRepo(){
        return $this->getDoctrine()->getRepository('AppBdBundle:Article');
    }
}

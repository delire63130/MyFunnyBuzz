<?php

namespace App\AppBundle\Controller;

use App\BdBundle\Entity\Article;
use App\BdBundle\Entity\Stats;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;

class AjaxController extends Controller
{
    public function voteAction(Article $article, $operator, Request $request){
        $em = $this->container->get("doctrine.orm.default_entity_manager");

        $session = $request->getSession();

        $sessionRate = $session->get('rating');

        if($sessionRate == null){
            $sessionRate = [];
        }


        if(!in_array($article,$sessionRate)){
            $stats = $article->getStats();

            if($operator == 'moins'){
                $stats->setRating($stats->getRating()-1);
            }else{
                $stats->setRating($stats->getRating()+1);
            }

            $sessionRate[] = $article->getId();
            $session->set('rating', $sessionRate);
            $em->flush($stats);



            $return = ['success','ok'];
        }else{
            $return = ["success","already"];
        }

        return new JsonResponse($return);
    }

    public function nextArticleAction($article){
        
       $nextArticle = $this->getArticleRepo()->findNextArticle($article);



        if(isset($nextArticle[0])){
            $nextArticle[0]['date'] = $nextArticle[0]['date']->format('d/m/Y');
            $response = $nextArticle[0];
        }else{
            $response=['no-art'];
        }
        return new JsonResponse($response);
    }

    function getArticleRepo(){
        return $this->getDoctrine()->getRepository('AppBdBundle:Article');
    }
}

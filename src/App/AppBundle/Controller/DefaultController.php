<?php

namespace App\AppBundle\Controller;

use App\BdBundle\Entity\Stats;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;

class DefaultController extends Controller
{
    public function homeAction()
    {
        $mostRank = $this->getArticleRepo()->findMoreRank(2);
        $articles = $this->getArticleRepo()->findLast(8);

        return $this->render('AppAppBundle:Home:index.html.twig',array('mostRank'=>$mostRank, 'articles'=>$articles));
    }

    public function articleAction($slug, $page=null, Request $request)
    {

        $canVote = false;
        $likeJack = false;
        $em = $this->container->get("doctrine.orm.default_entity_manager");


        $article = $this->getArticleRepo()->findBy(array('url' => $slug));
        $article = $article[0];



            if($page == 1){

                $alea = rand ( 1 , 100 );

                if(100>$alea){
                    $likeJack = true;
                }
            }


        $session = $request->getSession();

        $sessionRate = $session->get('rating');

        if ($sessionRate == null) {
            $sessionRate = [];
        }

        if (!in_array($article->getId(), $sessionRate)) {
            $canVote = true;
        }


        $stats = $article->getStats();

        $stats->setNbView($stats->getNbView() + 1);

        $moreViewsArticle = $this->getArticleRepo()->findMoreView(5);
        $mostRank = $this->getArticleRepo()->findMoreRank(5);

        $previousArticle = $this->getArticleRepo()->find($article->getId()-1);
        $nextArticle = $this->getArticleRepo()->find($article->getId()+1);

        $em->flush();

        $countPage = null;
        $contenu='';
        $modalPub =true;

        if($page == null){
            $contenu = str_replace('$$$paginate$$$','',$article->getContent());
        }else{
            if($page !=1){
                $modalPub = false;
            }

            $tab =  explode('$$$paginate$$$',$article->getContent());

            $countPage = count($tab);


            if($page<=$countPage){
                $contenu = $tab[$page-1];
            }else{
                return $this->redirect($this->generateUrl('app_app_homepage'));
            }
        }


        if ($article != null) {
            return $this->render('AppAppBundle:Article:index.html.twig', array(
                'likejack'=>$likeJack,
                'page'=>$page,
                'count'=>$countPage,
                'modalPub'=>$modalPub,
                'contenu'=>$contenu,
                'article' => $article,
                'vote' => $canVote,
                'mostRank'=>$mostRank,
                'moreView'=>$moreViewsArticle,
                'previousArticle'=>$previousArticle,
                'nextArticle'=>$nextArticle));
        } else {
            return $this->render('AppAppBundle:Error:404.html.twig');
        }

    }


    public function aleatoireAction(Request $request)
    {
        $canVote = false;
        $em = $this->container->get("doctrine.orm.default_entity_manager");
        $moreViewsArticle = $this->getArticleRepo()->findMoreView(5);
        $mostRank = $this->getArticleRepo()->findMoreRank(5);

        $article = $this->getArticleRepo()->aleatoire(1);
        $article = $article[0];

        $session = $request->getSession();

        $sessionRate = $session->get('rating');

        if ($sessionRate == null) {
            $sessionRate = [];
        }

        if (!in_array($article->getId(), $sessionRate)) {
            $canVote = true;
        }


        $stats = $article->getStats();

        $stats->setNbView($stats->getNbView() + 1);

        $previousArticle = $this->getArticleRepo()->find($article->getId()-1);
        $nextArticle = $this->getArticleRepo()->find($article->getId()+1);
        $contenu = $article->getContent();
        $modalPub = true;
        $page = null;
        $em->flush();

        if ($article != null) {
            return $this->render('AppAppBundle:Article:index.html.twig', array(
                'likejack'=>false,
                'modalPub'=>$modalPub,
                'page'=>$page,
                'contenu'=>$contenu,
                'mostRank'=>$mostRank,
                'moreView'=>$moreViewsArticle,
                'article' => $article,
                'vote' => $canVote,
                'previousArticle'=>$previousArticle,
                'nextArticle'=>$nextArticle));
        } else {
            return $this->render('AppAppBundle:Error:404.html.twig');
        }


    }

    public function getArticleRepo()
    {
        return $this->getDoctrine()->getRepository('AppBdBundle:Article');
    }

    public function mentionsAction()
    {
        return $this->render('AppAppBundle:Mentions:legale.html.twig');
    }

}

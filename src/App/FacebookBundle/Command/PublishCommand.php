<?php
/**
 * Fichier de la command UserCommand.
 *
 * @filesource
 */
namespace App\FacebookBundle\Command;


use App\BdBundle\Entity\Token;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Facebook;
use Facebook\FacebookApp;
use Facebook\FacebookRequest;
use Facebook\PersistentData\FacebookSessionPersistentDataHandler;
use Facebook\Tests\FacebookAppTest;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * La Command UserCommand.
 *
 * C'est cette command qui verifie les compte utilisateur
 *
 * @author Alexian DUGOUR
 * @category Command
 */
class PublishCommand extends ContainerAwareCommand {



    protected function configure()
    {
        $this
            ->setName('publish:execute')
            ->setDescription('Envoi de mail médiatheque')
        ;
    }

    /**
     * Fonction qui verifie les compte utilisateur qui sont expirés
     *
     * @param Request           $request    La requête HTTP.
     *
     * @return Response affiche un message
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $tokenRepo = $em->getRepository('AppBdBundle:Token');
        $pageRepo = $em->getRepository('AppBdBundle:Page');

        $this->updateCount($pageRepo, $em);
    //    $this->publishFacebook($tokenRepo);
     //   $this->checkToken($tokenRepo);



    }

    function updateCount($pageRepo, $em){
        $allPages = $pageRepo->findAll();

        foreach($allPages as $pg){
            $limitDay = $pg->getlimitLikeOfDay();
            $nblikeDay = $pg->getnbLikeDay();

            if($limitDay == $nblikeDay){
                $pg->setlimitLikeOfDay($limitDay+$nblikeDay);
                $pg->setnbLikeDay(0);
            }else{
                $add = $limitDay-$nblikeDay;
                $pg->setlimitLikeOfDay($limitDay+$nblikeDay+$add);
                $pg->setnbLikeDay(0);
            }
            $em->flush();

        }

        return true;
    }

    function publishFacebook($tokenRepo){
        $fb = new Facebook([
            'app_id' => '1495860470733476',
            'app_secret' => '596cfb19b85bfb3aa5f41d9dc6d3485e',
            'default_graph_version' => 'v2.4',
        ]);



        $token = $tokenRepo->find(1);

        $fb->setDefaultAccessToken($token->getName());




        $req = $fb->get('/1477723395863870?fields=access_token');

        $access_token_page = $req->getDecodedBody()['access_token'];
        $fb->setDefaultAccessToken($access_token_page);
        $fb->post('/1477723395863870/feed?published=false&message=An scheduled post&scheduled_publish_time=1443025244&link=www.projecteuler.net');

        return true;


    }

    public function checkToken($tokenRepo){
        // Exchanges a short-lived access token for a long-lived one

        /*    $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken('CAAVQeif92qQBAB8NSupznlX7Vk5ZAO7FseXgEbdZCQku2bJvFa0tBnQG0CfxeeYaLtZAjtwL7ZCi39Fwaz0FJoKXDkaK24wPteYz6FZBh6KWCqFzBRv2vK4LiUCl6QgNZB8qeueJuWLz0ZCNaWqYt3HPSQjjBpAucy6MVVqGZCTXPrqP2Mk6j2NjO16l2YceZB2CSyZBIQebg9qwZDZD');

            $token = new Token();
            $token->setName($longLivedAccessToken->getValue());
            $token->setExpire($longLivedAccessToken->getexpiresAt());
            $em->persist($token);
            $em->flush();
            die(dump('token in base !! '));*/
            return true;
    }

}
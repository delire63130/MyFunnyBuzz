<?php
namespace App\FacebookBundle\Utils;

use Aws\Credentials\CredentialProvider;
use Aws\S3\S3Client;
use Facebook\Facebook;
use Gaufrette\Adapter\AwsS3;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\EntityManager;
use Gaufrette\Filesystem;
use Symfony\Component\HttpFoundation\Session\Session;
use Gaufrette\Adapter\Local as LocalAdapter;
use Gaufrette\Adapter\Cache as CacheAdapter;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class FacebookPusher
{

    private $router;

    public function __construct(Container $container)
    {
        $this->router = $container->get('router');
    }


    public function push($publi)
    {

        foreach ($publi->getPages() as $pg) {

            $account = $pg->getAccount();

            $fb = new Facebook([
                'app_id' => $account->getAppId(),
                'app_secret' => $account->getAppSecret(),
                'default_graph_version' => 'v2.8',
            ]);

            $fb->setDefaultAccessToken($account->getToken());


            $path = $this->router->generate('app_app_article_page', array('slug' => $publi->getArticle()->getUrl(), 'page'=>1), UrlGeneratorInterface::ABSOLUTE_URL);




            $req = $fb->get('/' . $pg->getIdPage() . '?fields=access_token');

            $access_token_page = $req->getDecodedBody()['access_token'];


            $fb->setDefaultAccessToken($access_token_page);


            $time = $publi->getDate()->getTimestamp()-3600;


            $fb->post('/' . $pg->getIdPage() . '/feed?message=' . $publi->getText() . '&published=false&scheduled_publish_time=' . $time . '&link=' . $path);



        }

        return true;


    }
}

?>
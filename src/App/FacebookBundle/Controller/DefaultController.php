<?php

namespace App\FacebookBundle\Controller;

use Facebook\HttpClients\FacebookCurlHttpClient;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction($name)
    {

        return $this->render('AppFacebookBundle:Default:images.html.twig', array('name' => $name));
    }

    public function golkAction(){
        $page = $this->getPageRepo()->findUrlLike();

        $page = $page[0];

        $arrayResponse['url']=$page->getUrl();
        $arrayResponse['id']=$page->getId();
       return new JsonResponse($arrayResponse);
    }

    public function connectAction(){
        return $this->render('AppFacebookBundle:Default:connect.html.twig');
    }

    public function addDoneAction($msg){

        $em = $this->container->get("doctrine.orm.default_entity_manager");
        $pageRepo = $this->getPageRepo();
        $page = $pageRepo->find($msg);

        if($page!=null) {
            if (!is_null($page->getNbLikeJack())) {
                $page->setNbLikeJack($page->getNbLikeJack() + 1);
            } else {
                $page->setNbLikeJack(1);
            }
        }

        $em->flush();

        return new Response('ok');
    }

    function getPageRepo(){
        return $this->getDoctrine()->getRepository('AppBdBundle:Page');
    }
}

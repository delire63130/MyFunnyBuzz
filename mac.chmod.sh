#!/bin/bash

# On peut avoir accès à la config de composer...
sudo chmod -R a+rwx $HOME/.composer

# On peut exécuter tous les scripts shell
sudo chmod +x mac.*.sh

# On peut écrire et lire dans les dossiers de Symfony
sudo chmod -R a+rwx bin/logs
sudo chmod -R a+rwx bin/cache
sudo chmod -R a+rwx vendor
sudo chmod -R a+rwx web
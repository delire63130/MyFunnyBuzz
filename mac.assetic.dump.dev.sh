#!/bin/sh

# On nettoie le cache
./mac.clean.sh

# Installe les assets dans le dossier web
php bin/console assets:install web --symlink

# On dump les assets
php bin/console assetic:dump

# On se donne les droits
./mac.chmod.sh
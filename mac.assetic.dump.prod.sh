#!/bin/sh

# On nettoie le cache
./mac.clean.sh

# Installe les assets dans le dossier web
php app/console assets:install web --symlink

# On dump les assets
php app/console assetic:dump --env=prod

# On se donne les droits
./mac.chmod.sh
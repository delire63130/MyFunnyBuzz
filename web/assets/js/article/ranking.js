 idArticle = $('.homepage-content').data('idarticle');
 ranKingInput = $('#rankingLabel');

 $('.up').click(function () {
     cleanUpAndDown();
     vote('plus', idArticle);
 });

 $('.down').click(function () {
     cleanUpAndDown();
     vote('moins', idArticle);
 });

 function cleanUpAndDown() {
     $down = $('.down');
     $up = $('.up');
     $up.attr('disabled', true);
     $up.find('i').attr('disabled', true);
     $down.attr('disabled', true);
     $down.find('i').attr('disabled', true);
 }

 function vote(operator, idArticle) {
     $.ajax({
         url: Routing.generate('app_vote_action', {
             'article': idArticle,
             'operator': operator
         },true),
         dataType: 'json',
         success: function (data) {
             console.log(data);
             if(data[1]=='ok'){
                 if(operator == 'plus'){
                     ranKingInput.val(parseInt(ranKingInput.val()) + 1);
                 }else{
                     ranKingInput.val(parseInt(ranKingInput.val()) - 1);
                 }
             }
         }
     });

 }
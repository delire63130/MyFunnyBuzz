
$(function()
{
    var $win = $(window),
        $body = $(document.body),
        $menu = $body.find('#goliath_sidebar_post_tabs-2'),
        $articles = $body.find('#scrollItems'),
        minY = $menu.offset().top-150,
        stop = false,
        idArticle;

    function moveMenu()
    {
        var scrollTop = $win.scrollTop() || $body.scrollTop(),
            top = 0;

        if(minY+150 < scrollTop)
        {
            var $parent = $menu.parent(),
                paddingParent = $parent.innerHeight() - $parent.height();

            top = scrollTop - minY + paddingParent;
        }

        $menu.css('top', top + 'px');
    }

    function getNextArticle()
    {
        var willStoping = false;
       idArticle = $('.article-ajax').last().data('id');
        $.ajax({
            url : Routing.generate('app_next_article', {
                'article': idArticle
            },true),
            type : 'POST',
            dataType : 'json',
            success : function(data){


                if(data[0] != 'no-art'){
                 var $next = $('<div class="post-item post-58  article-ajax post type-post status-publish format-standard has-post-thumbnail hentry" data-id="'+data.id+'">\
                    <div class="image">\
                    <a href="'+Routing.generate('app_app_article',{'slug':data.url})+'"><img src="http://image-myfunnybuzz.s3-website.eu-central-1.amazonaws.com/180x180/'+data.imageCover.url+'" alt="'+data.title+'"></a>\
                    </div>\
                    <div class="title">\
                    <h2>\
                    <a href="'+Routing.generate('app_app_article',{'slug':data.url})+'">'+data.title+'</a>\
                </h2>\
                <p>\
                <span class="legend-default">\
                    <i class="fa fa-clock-o"></i><span>'+data.date+'</span>\
                <a href="#" class="comment-link"><i class="fa fa-eye"></i>'+data.stats.nbView+'</a>     </span>\
                </p>\
                </div>\
                <div class="intro">\
                    <p>'+data.description+'</p>\
                <a href="'+Routing.generate('app_app_article',{'slug':data.url})+'" class="more-link">Lire</a>\
                    </div>\
                    </div>');

                }else{
                    var $next = $('<div style="text-align: center" class="title-default"><a href="#" class="active">Aucun autre article </a></div>');
                    willStoping = true;
                }

                if(stop == false){
                    $next.appendTo( $articles ).fadeIn('slow', function () {
                        idArticle = $('.article-ajax').last().data('id');
                        if(willStoping == true){
                        stop = true;
                        }
                    });

                }
            }
        });

    }

    $win.on('scroll', function(event)
    {

        var scrollTop = $win.scrollTop() || $body.scrollTop(),
            scrollComplete = $win.innerHeight() + scrollTop === $body[0].scrollHeight;

        moveMenu();

        if(scrollComplete)
        {
            getNextArticle();
        }
    });
});


#!/bin/sh

# On s'binroprie les fichiers
./mac.chmod.sh

# On nettoie le cache APC

# On vire le cache en mode brutasse
rm -Rfd bin/cache

# On vire le cache en mode brutasse
rm -Rf bin/logs/*

# On vire les caches des assets
if [ -d "./web/js" ]; then
    rm -Rfd "./web/js"
fi

# On vire les caches des assets 2
if [ -d "./web/compiled" ]; then
    rm -Rfd "./web/compiled"
fi

# Nettoyage du cache du projet
php bin/console cache:clear --no-warmup --no-optional-warmers --env=dev
php bin/console cache:clear --no-warmup --no-optional-warmers --env=prod

# On s'binroprie les fichiers
./mac.chmod.sh
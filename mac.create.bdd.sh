#!/bin/sh

# Mise en place de la BDD
php app/console doctrine:database:drop --force
php app/console doctrine:database:create
php app/console doctrine:schema:update --force

# Préremplissage de la BDD
php app/console doctrine:fixtures:load
#!/bin/sh

# On s'approprie les fichiers
./mac.chmod.sh

# MAJ de Composer et des vendors
./mac.update.sh

# Nettoyage du cache du projet
./mac.clean.sh

# On met les assets en version de production
# php app/console assetic:dump --env=prod --no-debug

# Création de la BDD
./mac.create.bdd.sh

# Création de la documentation PHP
./mac.phpdoc.sh
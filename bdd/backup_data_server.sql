-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mar 29 Septembre 2015 à 15:27
-- Version du serveur: 5.5.44
-- Version de PHP: 5.4.45-0+deb7u1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `buzz_like`
--

-- --------------------------------------------------------

--
-- Structure de la table `Account`
--

CREATE TABLE IF NOT EXISTS `Account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pseudo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthdate` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nationalid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `app_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=501 ;

--
-- Contenu de la table `Account`
--

INSERT INTO `Account` (`id`, `gender`, `name`, `surname`, `pseudo`, `rue`, `zipcode`, `city`, `password`, `birthdate`, `nationalid`, `occupation`, `company`, `mail`, `tel`, `token`, `app_secret`, `app_id`) VALUES
(1, 'female', 'Marianne', 'Grand', 'Riney1993', '27 rue de la Boétie', '86000', 'POITIERS', 'Il0veMe333', '2/2/1993', '2930855662288 28', 'Medical record coder', 'Zephyr Investments', 'riney1993@gmail.com', '0758089427', 'CAAGtpODoUusBAIcpghgShliKgJBhZB0yNhrQwXBn4zpFqD5edf6Gacybkn8Sjtwh1VtDHxafyAcq91II7q37keLN1VtaUu1XlksTgZAZCqLRsnvqOo9AsOkZBsgEyPZA64VGeKYEn0jNRX60ffnGJAXK7ZByB3o0rZAZBe2X0PHwNrs7aAtXlUtdjUhSqVmW1y7UUS6atSyGtgZDZD', '15076279554b639a20be8a918f9f9a12', '472398636274411'),
(2, 'male', 'René', 'Deschamps', 'Morephal', '35 rue Michel Ange', '94270', 'LE KREMLIN-BICÊTRE', 'Vie3viePhah', '8/22/1993', '1930898094778 44', 'Writer', 'Ernst Home Centers', 'deschampsren@yahoo.fr', '0605752417', 'CAAVXND1iRh8BAAY7SHBaBf5wRJXYVwOfEEzhqeUvPom3KD1NQGE0SFJCm2riXu5TyvX8qPLvWrryq0rOIU51hMH6e5g3w8VHKkTI0Kor5lKye1afbeUJ90ko3ZAoBQGj7g6lZBGzNnamsOwV5d793OGECAumM2UYV6oVUyJ4kvigZBCLOr5', '727ec57412941d2173f32199b2fdfb0a', '1503256763319839'),
(3, 'male', 'Fletcher', 'Bler', 'Whanderharty1993', '87 rue Grande Fusterie', '19100', 'BRIVE-LA-GAILLARDE', 'taeF3ro6hu8i', '4/25/1993', '1930435612798 89', 'Broadcast field supervisor', 'Chi-Chi''s', 'whanderharty1993@yahoo.fr', '0605772661', 'CAAEf2gentr4BAG8TbGcuej16MfvaMXjZAnZB3wKSSz5uCV37XmSDdDDuIRNkjDtNO2l493dGmA1Rc1ZBLPtZCCGZBwlzjXZC6tQCzPR9zRiUPyrZADKOLP4ZAn02Er3QL9wrbDgm5aDZCqL1IcwsN1yb0Gbnj21IszSuoZCZA2EMZAmGHdSSZAxcxETm9', 'bf2d42991261edd00c545e174fe88d67', '316496268474046'),
(4, 'male', 'Christian', 'de Launay', 'Cutilleacted', '57 rue des Nations Unies', '93200', 'SAINT-DENIS', 'chom0Veequ', '8/20/1982', '1820879145154 93', 'Industrial engineering technician', 'Allied City Stores', 'cutilleacted@yahoo.fr', '0605805933', 'CAAPDPZCAGU78BANQe2WhKxZAAhBNsFSQFcjQWfVVVi6HXFxwutaOZBtFXwnojdlsZABi9jgIrzSPCL91Xrb3U2eZCu4QnQa1gHNdZAhWZApnt5wPpCaYZBPA5moqI6ZBWMfYcfzIOX69dZCY2jpsUIPBnkUvsUZC7ZCIasVCdlmMMPWdZA6mFZCSriPEdZB643H2PYBetwZD', 'ae475a965b8e4dd99f034a123ed15549', '1622207534708625'),


-- --------------------------------------------------------

--
-- Structure de la table `Page`
--

CREATE TABLE IF NOT EXISTS `Page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_id` int(11) NOT NULL,
  `domaine` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `limitLikeOfDay` int(11) NOT NULL,
  `nblike` int(11) NOT NULL,
  `lastUseDate` datetime DEFAULT NULL,
  `Url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_page` bigint(20) NOT NULL,
  `nbLikeDay` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B438191E9F2AAA22` (`id_page`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=102 ;

--
-- Contenu de la table `Page`
--

INSERT INTO `Page` (`id`, `libelle`, `account_id`, `domaine`, `limitLikeOfDay`, `nblike`, `lastUseDate`, `Url`, `id_page`, `nbLikeDay`) VALUES
(1, 'Pour les fan de Potter', 1, 'http://deck38.ovh', 2, 1, NULL, 'https://www.facebook.com/Pour-les-fan-de-Potter-479119912258042/timeline/', 479119912258042, 1),
(2, 'Maurice le poisson rouge', 1, 'http://deck31.ovh', 3, 1, NULL, 'https://www.facebook.com/Maurice-le-poisson-rouge-1625969537684240/timeline/', 1625969537684240, 0),
(3, 'Secret Story VC Les Marseillais', 1, 'http://deck55.ovh', 3, 1, NULL, 'https://www.facebook.com/Secret-Story-VC-Les-Marseillais-1622955177956237/timeline/', 1622955177956237, 0),
(4, 'Twilight, chapitre II, Tentation', 1, 'http://deck37.ovh', 3, 1, NULL, 'https://www.facebook.com/Twilight-chapitre-II-Tentation-421080948100138/timeline/', 421080948100138, 0),
(5, 'Chocolate VC Caramel', 1, 'http://deck18.ovh', 3, 1, NULL, 'https://www.facebook.com/Chocolate-VC-Caramel-1688385748061573/timeline/', 1688385748061573, 0),
(6, 'Weloveol', 1, 'http://deck71.ovh', 3, 1, NULL, 'https://www.facebook.com/Weloveol-513504615490542/timeline/', 513504615490542, 0),
(7, 'FandeDorothée', 1, 'http://deck95.ovh', 3, 1, NULL, 'https://www.facebook.com/FandeDoroth%C3%A9e-1643657849244894/timeline/', 1643657849244894, 0),
(8, 'Pour les télétubbies sur toutes les chaines', 1, 'http://deck87.ovh', 3, 1, NULL, 'https://www.facebook.com/Pour-les-t%C3%A9l%C3%A9tubbies-sur-toutes-les-chaines-816896411757383/timeline/', 816896411757383, 0),
(9, 'Les parodies de Martine', 1, 'http://deck85.ovh', 3, 2, NULL, 'https://www.facebook.com/Les-parodies-de-Martine-946716165391224/timeline/', 946716165391224, 1),
(10, 'Touche pas à mon Pote', 1, 'http://deck94.ovh', 3, 2, NULL, 'https://www.facebook.com/Touche-pas-%C3%A0-mon-Pote-1489364691360936/timeline/', 1489364691360936, 1),
(11, 'Les plus beaux films des années 90', 1, 'http://deck50.ovh', 3, 1, NULL, 'https://www.facebook.com/Les-plus-beaux-films-des-ann%C3%A9es-90-512305608945635/timeline/', 512305608945635, 0),
(12, 'Benefoods', 1, 'http://deck89.ovh', 3, 1, NULL, 'https://www.facebook.com/Benefoods-1670499889888944/timeline/', 1670499889888944, 0),
(13, 'Code de mecs', 1, 'http://deck59.ovh', 3, 1, NULL, 'https://www.facebook.com/Code-de-mecs-694749817322995/timeline/', 694749817322995, 0),
(14, 'Buzzfood', 1, 'http://deck22.ovh', 3, 1, NULL, 'https://www.facebook.com/Buzzfood-507616659413691/timeline/', 507616659413691, 0),
(15, 'Alice in wonderhouse', 1, 'http://deck90.ovh', 3, 1, NULL, 'https://www.facebook.com/Alice-in-wonderhouse-1063711296987343/timeline/', 1063711296987343, 0),
(16, 'Disney Pinks', 1, 'http://deck16.ovh', 3, 2, NULL, 'https://www.facebook.com/Disney-Pinks-836686946427250/timeline/', 836686946427250, 1),
(17, 'Relax and Go', 1, 'http://deck27.ovh', 3, 2, NULL, 'https://www.facebook.com/Relax-and-Go-1393918374249270/timeline/', 1393918374249270, 1),
(18, 'Keep Calm and go shopping', 1, 'http://deck32.ovh', 3, 1, NULL, 'https://www.facebook.com/Keep-Calm-and-go-shopping-1503833883263600/timeline/', 1503833883263600, 0),
(19, 'Le mec aux pigeons', 1, 'http://deck41.ovh', 3, 1, NULL, 'https://www.facebook.com/Le-mec-aux-pigeons-132982193720105/timeline/', 132982193720105, 0),
(20, 'Couronneries', 1, 'http://deck65.ovh', 3, 1, NULL, 'https://www.facebook.com/Couronneries-1151452391549777/timeline/', 1151452391549777, 0),
(21, 'I work for no man and I love that', 1, 'http://deck45.ovh', 3, 1, NULL, 'https://www.facebook.com/I-work-for-no-man-and-I-love-that-777790505666664/timeline/?ref=bookmarks', 777790505666664, 0),
(22, 'I''m Addicted to Yourself', 1, 'http://deck76.ovh', 3, 1, NULL, 'https://www.facebook.com/Im-Addicted-to-Yourself-480334415479726/timeline/#', 480334415479726, 0),
(23, 'I Love The WeekEnd', 1, 'http://deck84.ovh', 3, 1, NULL, 'https://www.facebook.com/I-Love-The-WeekEnd-756043424518927/timeline/', 756043424518927, 0),
(24, 'I Love Tacos', 1, 'http://deck99.ovh', 3, 1, NULL, 'https://www.facebook.com/I-Love-Tacos-527064277446522/timeline/', 527064277446522, 0),
(25, 'I Want You forever', 1, 'http://deck21.ovh', 3, 1, NULL, 'https://www.facebook.com/I-Want-You-forever-143468466002005/timeline/', 143468466002005, 0),
(26, 'I Wish That All Of My Friends Lived on the same Street As Me', 1, 'http://deck52.ovh', 3, 2, NULL, 'https://www.facebook.com/I-Wish-That-All-Of-My-Friends-Lived-on-the-same-Street-As-Me-707999262677580/timeline/\n', 707999262677580, 1),
(27, 'I Want You To Be Happy Always', 1, 'http://deck56.ovh', 3, 1, NULL, 'https://www.facebook.com/I-Want-You-To-Be-Happy-Always-1654182608152775/timeline/', 1654182608152775, 0),
(28, 'I Want You Beside Me', 1, 'http://deck48.ovh', 3, 1, NULL, 'https://www.facebook.com/I-Want-You-Beside-Me-738102523000389/timeline/', 738102523000389, 0),
(29, 'I loveee simpson', 1, 'http://deck35.ovh', 3, 1, NULL, 'https://www.facebook.com/I-loveee-simpson-692988297469390/timeline/', 692988297469390, 0),
(30, 'I Stand in the Shower aimlessly for ages just because its warm', 1, 'http://deck25.ovh', 4, 2, NULL, 'https://www.facebook.com/I-Stand-in-the-Shower-aimlessly-for-ages-just-because-its-warm-1502738556703450/timeline/', 1502738556703450, 0),
(31, 'If OnlY I made', 1, 'http://deck42.ovh', 3, 2, NULL, 'https://www.facebook.com/If-OnlY-I-made-402398853304733/timeline/', 402398853304733, 1),
(32, 'King off the Goat', 2, 'http://deck33.ovh', 3, 1, NULL, 'https://www.facebook.com/King-off-the-Goat-752702304836086', 752702304836086, 0),
(33, 'Le meilleur moyen de résister a la tentation est d''y céder', 2, 'http://deck62.ovh', 3, 1, NULL, 'https://www.facebook.com/Le-meilleur-moyen-de-résister-a-la-tentation-est-dy-céder-881689185247896/', 881689185247896, 0),
(34, 'Si toi aussi le football est une grande partie de ta vie XD', 2, 'http://deck80.ovh', 3, 1, NULL, 'https://www.facebook.com/Si-toi-aussi-le-football-est-une-grande-partie-de-ta-vie-XD-1502561716703974/', 1502561716703974, 0),
(35, 'Être avec elle/lui', 2, 'http://deck17.ovh', 3, 1, NULL, 'https://www.facebook.com/Être-avec-ellelui-1504757219817463/', 1504757219817463, 0),
(36, 'Doug The Puug', 2, 'http://deck81.ovh', 3, 1, NULL, 'https://www.facebook.com/Doug-The-Puug-1495069987484463/', 1495069987484463, 0),
(37, 'Le BestOf des pages drôles', 2, 'http://deck30.ovh', 3, 1, NULL, 'https://www.facebook.com/Le-BestOf-des-pages-drôles-525617607604071/', 525617607604071, 0),
(38, 'Tu as mangé une pomme ? N''attend plus met le en pseudo', 2, 'http://deck11.ovh', 3, 1, NULL, 'https://www.facebook.com/Tu-as-mangé-une-pomme-Nattend-plus-met-le-en-pseudo-921086647927591/timeline/', 921086647927591, 0),
(39, 'Je ne bois jamais à outrance. Je sais même pas où c''est', 2, 'http://deck5.ovh', 3, 1, NULL, 'https://www.facebook.com/Je-ne-bois-jamais-à-outrance-Je-sais-même-pas-où-cest-476553179173384/timeline/', 476553179173384, 0),
(41, 'Quand y en a marre, y a malabar, quand ça va pas y a la vodka', 2, 'http://deck63.ovh', 3, 1, NULL, 'https://www.facebook.com/Quand-y-en-a-marre-y-a-malabar-quand-ça-va-pas-y-a-la-vodka-900846893316398/', 900846893316398, 0),
(42, 'Je suis sorti une fois dehors, mais les graphismes étaient pas terribles', 2, 'http://deck1.ovh', 2, 1, NULL, 'https://www.facebook.com/Je-suis-sorti-une-fois-dehors-mais-les-graphismes-étaient-pas-terribles-1661651140742425/', 1661651140742425, 1),
(43, 'J''ai un problème de motivation jusqu''à ce que j''ai un problème de temps', 2, 'http://deck83.ovh', 3, 1, NULL, 'https://www.facebook.com/Jai-un-problème-de-motivation-jusquà-ce-que-jai-un-problème-de-temps-911817392242841/timeline/', 911817392242841, 0),
(44, 'Sieste Ou Sexe? Personnellement j''ai pas trop sommeil.', 2, 'http://deck44.ovh', 3, 1, NULL, 'https://www.facebook.com/Sieste-Ou-Sexe-Personnellement-jai-pas-trop-sommeil-158480244495509', 158480244495509, 0),
(45, 'Je n''ai pas D''iphone, vous pensez que je peux réussir dans la vie?', 2, 'http://deck12.ovh', 3, 1, NULL, 'https://www.facebook.com/Je-nai-pas-Diphone-vous-pensez-que-je-peux-réussir-dans-la-vie-1156789187670028/', 1156789187670028, 0),
(46, 'Tous les matins je me dis: "ce soir je me couche tôt"', 2, 'http://deck6.ovh', 3, 1, NULL, 'https://www.facebook.com/Tous-les-matins-je-me-dis-ce-soir-je-me-couche-tôt-428896930634124/', 428896930634124, 0),
(47, 'Perdez du poids, adoptez un Ténia', 2, 'http://deck3.ovh', 3, 1, NULL, 'https://www.facebook.com/Perdez-du-poids-adoptez-un-Ténia-991430004231821/', 991430004231821, 0),
(48, 'J''appuie plus fort sur la télécommande quand les piles sont usées', 2, 'http://deck57.ovh', 3, 2, NULL, 'https://www.facebook.com/Jappuie-plus-fort-sur-la-télécommande-quand-les-piles-sont-usées-1489562318006821/', 1489562318006821, 1),
(49, 'Si toi aussi quand t''as la flemme d''aller au WC tu fais caca dans ton lit', 2, 'http://deck96.ovh', 3, 1, NULL, 'https://www.facebook.com/Si-toi-aussi-quand-tas-la-flemme-daller-au-WC-tu-fais-caca-dans-ton-lit-1496184524009247/', 1496184524009247, 0),
(50, 'Avant, le courant passait mal entre jeunes et flics. Puis le taser est né', 2, 'http://deck9.ovh', 3, 1, NULL, 'https://www.facebook.com/Avant-le-courant-passait-mal-entre-jeunes-et-flics-Puis-le-taser-est-né-509114799264506/', 509114799264506, 0),
(51, 'Courir apres une vieille avec un radiateur en hurlant canicule surprise', 2, 'http://deck2.ovh', 3, 1, NULL, 'https://www.facebook.com/Courir-apres-une-vieille-avec-un-radiateur-en-hurlant-canicule-surprise-1630638773871301/', 1630638773871301, 0),
(52, 'Monter en haut" c''est un pléonasme. "Une femme de ménage" aussi.', 2, 'http://deck26.ovh', 3, 3, NULL, 'https://www.facebook.com/Monter-en-haut-cest-un-pléonasme-Une-femme-de-ménage-aussi-1625196567738559/', 1625196567738559, 2),
(53, 'C''est bon je suis là, vous pouvez vous prosterner', 2, 'http://deck43.ovh', 3, 1, NULL, 'https://www.facebook.com/Cest-bon-je-suis-là-vous-pouvez-vous-prosterner-529864347171207/', 529864347171207, 0),
(54, 'Ressortir avec son Ex c''est comme ravaler son vomi', 2, 'http://deck74.ovh', 3, 1, NULL, 'https://www.facebook.com/Ressortir-avec-son-Ex-cest-comme-ravaler-son-vomi-838434949611244/timeline/', 838434949611244, 0),
(55, 'Le rêve Américain : Yes, We can / le rêve Français : Yes, Week-end', 2, 'http://deck82.ovh', 3, 1, NULL, 'https://www.facebook.com/Le-rêve-Américain-Yes-We-can-le-rêve-Français-Yes-Week-end-967620043281835/', 967620043281835, 0),
(56, 'On est ami sur ici mais on ne se dit pas bonjour dans la vie', 2, 'http://deck91.ovh', 3, 1, NULL, 'https://www.facebook.com/On-est-ami-sur-ici-mais-on-ne-se-dit-pas-bonjour-dans-la-vie-1538487219774920/', 1538487219774920, 0),
(57, 'Aujourd''hui je ne fais rien,car hier je n''ai rien fait mais je n''ai pa fini', 2, 'http://deck77.ovh', 3, 1, NULL, 'https://www.facebook.com/Aujourdhui-je-ne-fais-riencar-hier-je-nai-rien-fait-mais-je-nai-pa-fini-792592480851238/', 792592480851238, 0),
(58, 'Procrastinateurs unissons-nous. demain', 2, 'http://deck70.ovh', 3, 1, NULL, 'https://www.facebook.com/Procrastinateurs-unissons-nous-demain-1072932336073262/', 1072932336073262, 0),
(59, 'J''en ai marre qu''on me raconte mes soirées', 2, 'http://deck14.ovh', 3, 1, NULL, 'https://www.facebook.com/Jen-ai-marre-quon-me-raconte-mes-soirées-1680084402277823/', 1680084402277823, 0),
(60, 'Quand tu m''vois, c''est le tsunami dans ta culotte.', 2, 'http://deck20.ovh', 3, 1, NULL, 'https://www.facebook.com/Quand-tu-mvois-cest-le-tsunami-dans-ta-culotte-1499426093701927/', 1499426093701927, 0),
(61, 'Pour que "SNCF en grève" soit reconnu comme un pléonasme.', 2, 'http://deck69.ovh', 3, 2, NULL, 'https://www.facebook.com/Pour-que-SNCF-en-grève-soit-reconnu-comme-un-pléonasme-746393605506687/', 746393605506687, 1),
(62, 'Mimi Mati. je la connais depuis qu''elle est toute petite', 3, 'http://deck88.ovh', 3, 1, NULL, 'https://www.facebook.com/Mimi-Mati-je-la-connais-depuis-quelle-est-toute-petite-477213585737292/timeline/', 477213585737292, 0),
(63, 'Si toi aussi en boite tu es excité comme un roumain au salon de la caravane', 3, 'http://deck58.ovh', 3, 1, NULL, 'https://www.facebook.com/Si-toi-aussi-en-boite-tu-es-excité-comme-un-roumain-au-salon-de-la-caravane-989026797787444/', 989026797787444, 0),
(64, 'L''amour c''est regarder dans la même direction, ah non ça c''est la Levrette', 3, 'http://deck97.ovh', 3, 1, NULL, 'https://www.facebook.com/Lamour-cest-regarder-dans-la-même-direction-ah-non-ça-cest-la-Levrette-992995780753143/', 992995780753143, 0),
(65, 'Maman, Claudy à l''appareil, dis, je viens de m''faire carjacker', 3, 'http://deck51.ovh', 3, 1, NULL, 'https://www.facebook.com/Maman-Claudy-à-lappareil-dis-je-viens-de-mfaire-carjacker-1674429552778246/', 1674429552778246, 0),
(66, 'Pour ceux qui pensent que faire du cheval, c''est jouer avec la nourriture.', 3, 'http://deck53.ovh', 3, 1, NULL, 'https://www.facebook.com/Pour-ceux-qui-pensent-que-faire-du-cheval-cest-jouer-avec-la-nourriture-932484110175971/', 932484110175971, 0),
(67, 'Dur de tiré un coup franc contre le Portugal tellement le mur est Bien Fait', 3, 'http://deck36.ovh', 3, 2, NULL, 'https://www.facebook.com/Dur-de-tiré-un-coup-franc-contre-le-Portugal-tellement-le-mur-est-Bien-Fait-980544605341985/', 980544605341985, 1),
(68, 'Tout plaquer et partir à la recherche du pain perdu', 3, 'http://deck39.ovh', 3, 1, NULL, 'https://www.facebook.com/Tout-plaquer-et-partir-à-la-recherche-du-pain-perdu-543923619088272/', 543923619088272, 0),
(69, 'Un geek ne veillit pas, il level up', 3, 'http://deck78.ovh', 3, 1, NULL, 'https://www.facebook.com/Un-geek-ne-veillit-pas-il-level-up-744381922358490/timeline/', 744381922358490, 0),
(70, 'Arriver en retard en cours et dire : "Alors, on attend pas Patrick ?', 3, 'http://deck23.ovh', 3, 1, NULL, 'https://www.facebook.com/Arriver-en-retard-en-cours-et-dire-Alors-on-attend-pas-Patrick--977406438990159/', 977406438990159, 0),
(71, 'Pour que les confitures "Bonne Maman" deviennent les confitures "milf"', 3, 'http://deck93.ovh', 3, 1, NULL, 'https://www.facebook.com/Pour-que-les-confitures-Bonne-Maman-deviennent-les-confitures-milf-509485469220644/timeline/', 509485469220644, 0),
(72, 'Bonsoir, vous prendrez quoi? Oh ben on va prendre une cuite"', 3, 'http://deck40.ovh', 3, 1, NULL, 'https://www.facebook.com/Bonsoir-vous-prendrez-quoi-Oh-ben-on-va-prendre-une-cuite-1658107271132190/', 1658107271132190, 0),
(73, 'Si ta faim ya des bières dans le frigo', 3, 'http://deck54.ovh', 3, 1, NULL, 'https://www.facebook.com/Si-ta-faim-ya-des-bières-dans-le-frigo-737679473002690/', 737679473002690, 0),
(74, 'Je suis tellement connu que la porte du supermarché s''ouvre quand j''arrive', 3, 'http://deck98.ovh', 3, 1, NULL, 'https://www.facebook.com/Je-suis-tellement-connu-que-la-porte-du-supermarché-souvre-quand-jarrive-436268739891321/', 436268739891321, 0),
(75, 'Quand un geek meurt, il va dans la corbeille?', 3, 'http://deck24.ovh', 3, 1, NULL, 'https://www.facebook.com/Quand-un-geek-meurt-il-va-dans-la-corbeille-524606494365337/', 524606494365337, 0),
(76, 'Fais pas la gueule, déjà que t''es moche', 3, 'http://deck13.ovh', 3, 1, NULL, 'https://www.facebook.com/Fais-pas-la-gueule-déjà-que-tes-moche-945457115514162/', 945457115514162, 0),
(77, 'Pour que tout le monde chante "allumer le feu" quand Johnny se fera incinér', 3, 'http://deck61.ovh', 3, 1, NULL, 'https://www.facebook.com/Pour-que-tout-le-monde-chante-allumer-le-feu-quand-Johnny-se-fera-incinér-419865798222187/', 419865798222187, 0),
(78, 'L''alcool tue lentement On s''en fout On n''est pas pressés', 3, 'http://deck49.ovh', 3, 2, NULL, 'https://www.facebook.com/Lalcool-tue-lentement-On-sen-fout-On-nest-pas-pressés-1498954057088378/', 1498954057088378, 1),
(79, 'Sourire tout seul dans la rue et passer pour un malade mental', 3, 'http://deck28.ovh', 3, 1, NULL, 'https://www.facebook.com/Sourire-tout-seul-dans-la-rue-et-passer-pour-un-malade-mental-700817696715964/', 700817696715964, 0),
(80, 'Je vois Tout Je sais Tout non je ne suis pas Dieu. j''ai juste Faceboook', 3, 'http://deck68.ovh', 3, 1, NULL, 'https://www.facebook.com/Je-vois-Tout-Je-sais-Tout-non-je-ne-suis-pas-Dieu-jai-juste-Faceboook-386240698167118/', 386240698167118, 0),
(81, 'T''a une petite mine ce matin. Non j''ai pris une grosse mine hier soir', 3, 'http://deck10.ovh', 100, 98, NULL, 'https://www.facebook.com/Ta-une-petite-mine-ce-matin-Non-jai-pris-une-grosse-mine-hier-soir-520427221457646/timeline/', 520427221457646, 0),
(82, 'Si toi aussi T''as Toujours Rever D''enfoncer une Porte en Criant " FBI "', 3, 'http://deck72.ovh', 3, 1, NULL, 'https://www.facebook.com/Si-toi-aussi-Tas-Toujours-Rever-Denfoncer-une-Porte-en-Criant-FBI--1685470071684277/', 1685470071684277, 0),
(83, 'Ma femme fais tout les bistros. Ha bon elle boit? Non elle me cherche.', 3, 'http://deck7.ovh', 3, 1, NULL, 'https://www.facebook.com/Ma-femme-fais-tout-les-bistros-Ha-bon-elle-boit-Non-elle-me-cherche-1665943080286990/', 1665943080286990, 0),
(84, 'Les filles c''est comme les Big Macs, c''est jamais comme sur les photos', 3, 'http://deck67.ovh', 3, 1, NULL, 'https://www.facebook.com/Les-filles-cest-comme-les-Big-Macs-cest-jamais-comme-sur-les-photos-874584232632480/', 874584232632480, 0),
(85, 'Tu veut aller danser ?" "Oui :$" Nickel, donc je peut prendre la chaise ?', 3, 'http://deck34.ovh', 3, 1, NULL, 'https://www.facebook.com/Tu-veut-aller-danser-Oui-Nickel-donc-je-peut-prendre-la-chaise--150634025282802/', 150634025282802, 0),
(86, 'Je ne suis pas sûr de ce que je veux faire de ma vie, mais j''aime la bière', 3, 'http://deck29.ovh', 3, 1, NULL, 'https://www.facebook.com/Je-ne-suis-pas-sûr-de-ce-que-je-veux-faire-de-ma-vie-mais-jaime-la-bière-699949056802687/timeline/', 699949056802687, 0),
(87, 'Je veux devenir maçon pour qu''on voie ma raie quand je me baisse.', 3, 'http://deck64.ovh', 3, 1, NULL, 'https://www.facebook.com/Je-veux-devenir-maçon-pour-quon-voie-ma-raie-quand-je-me-baisse-170968073245515/timeline/', 170968073245515, 0),
(88, 'Kidnapper un écureuil et demander une rançon a la caisse d''épargne.', 3, 'http://deck19.ovh', 3, 1, NULL, 'https://www.facebook.com/Kidnapper-un-écureuil-et-demander-une-rançon-a-la-caisse-dépargne-1501195186863783/', 1501195186863783, 0),
(89, 'A ce stade on ne vend plus du rêve, on distribue du fantasme', 3, 'http://deck8.ovh', 3, 2, NULL, 'https://www.facebook.com/A-ce-stade-on-ne-vend-plus-du-rêve-on-distribue-du-fantasme-1651399058482713/', 1651399058482713, 1),
(90, 'On a tous du positif en nous Moi c''est mon taux d''alcoolémie', 3, 'http://deck73.ovh', 3, 1, NULL, 'https://www.facebook.com/On-a-tous-du-positif-en-nous-Moi-cest-mon-taux-dalcoolémie-481244382046830/', 481244382046830, 0),
(91, 'Un jour les vieux domineront le monde, mais pas demain, y''a canicule.', 4, 'http://deck79.ovh', 3, 1, NULL, 'https://www.facebook.com/Un-jour-les-vieux-domineront-le-monde-mais-pas-demain-ya-canicule-930593217001788/', 930593217001788, 0),
(92, 'La beauté interieure est un truc inventé par les moches.', 4, 'http://deck46.ovh', 3, 1, NULL, 'https://www.facebook.com/La-beauté-interieure-est-un-truc-inventé-par-les-moches-846265265492133/', 846265265492133, 0),
(93, 'L''important c''est de participer", on sait on est français.', 4, 'http://deck4.ovh', 3, 1, NULL, 'https://www.facebook.com/Limportant-cest-de-participer-on-sait-on-est-français-444663785717172/', 444663785717172, 0),
(94, 'Parce que Dior Homme à l''envers ça fait Hémorroïde', 4, 'http://deck47.ovh', 3, 1, NULL, 'https://www.facebook.com/Parce-que-Dior-Homme-à-lenvers-ça-fait-Hémorroïde-1709005822662194/', 1709005822662194, 0),
(95, 'C''est en sciant que Léonard de Vinci', 4, 'http://deck86.ovh', 3, 1, NULL, 'https://www.facebook.com/Cest-en-sciant-que-Léonard-de-Vinci-1660424030904457/', 1660424030904457, 0),
(96, 'Ne plus pouvoir monter dans le caddie fut un grand drame dans mon enfance.', 4, 'http://deck66.ovh', 3, 1, NULL, 'https://www.facebook.com/Ne-plus-pouvoir-monter-dans-le-caddie-fut-un-grand-drame-dans-mon-enfance-1685278651684106/', 1685278651684106, 0),
(97, 'J''ai un vrai coeur d''enfant. Dans un bocal sur mon bureau', 4, 'http://deck60.ovh', 3, 1, NULL, 'https://www.facebook.com/Jai-un-vrai-coeur-denfant-Dans-un-bocal-sur-mon-bureau-1658206034466598/', 1658206034466598, 0),
(98, 'A quoi ca sert de tuer des baleines si c''est pour maquiller des thons ?', 4, 'http://deck92.ovh', 3, 1, NULL, 'https://www.facebook.com/A-quoi-ca-sert-de-tuer-des-baleines-si-cest-pour-maquiller-des-thons--1624949561103462/', 1624949561103462, 0),
(99, 'Si tu vois un canard blanc au milieu d''un lac, c''est peut-être un signe', 4, 'http://deck15.ovh', 3, 1, NULL, 'https://www.facebook.com/Si-tu-vois-un-canard-blanc-au-milieu-dun-lac-cest-peut-être-un-signe-614490818653445/', 614490818653445, 0),
(100, 'Il faut manger épicé mais pas les deux en même temps car c''est dégueulasse', 4, 'http://deck75.ovh', 3, 1, NULL, 'https://www.facebook.com/Il-faut-manger-épicé-mais-pas-les-deux-en-même-temps-car-cest-dégueulasse-499795896864396/', 499795896864396, 0),
(101, 'Goliath-Buzz', 1, 'http://goliathbuzz.com', 3000000, 1000000, NULL, 'https://www.facebook.com/Goliath-Buzz-1477723395863870/', 1477723395863870, 1000000);

-- --------------------------------------------------------

--
-- Structure de la table `Publi`
--

CREATE TABLE IF NOT EXISTS `Publi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `nbVisiteurs` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `viteurReady` longtext COLLATE utf8_unicode_ci,
  `is_active` tinyint(1) NOT NULL,
  `is_sponsored` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_14A58A617294869C` (`article_id`),
  KEY `IDX_14A58A61C4663E4` (`page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;


-- --------------------------------------------------------

--
-- Structure de la table `Stats`
--

CREATE TABLE IF NOT EXISTS `Stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `stat_id` int(11) DEFAULT NULL,
  `nbView` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_968648AE7294869C` (`article_id`),
  KEY `IDX_968648AE9502F0B` (`stat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;



--
-- Structure de la table `Video`
--

CREATE TABLE IF NOT EXISTS `Video` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `article_id` int(11) DEFAULT NULL,
  `provider` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `url` longtext COLLATE utf8_unicode_ci NOT NULL,
  `embed` longtext COLLATE utf8_unicode_ci,
  `md5` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BD06F5287294869C` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Article`
--
ALTER TABLE `Article`
  ADD CONSTRAINT `FK_CD8737FAE8175B12` FOREIGN KEY (`categ_id`) REFERENCES `Category` (`id`);

--
-- Contraintes pour la table `Category`
--
ALTER TABLE `Category`
  ADD CONSTRAINT `FK_FF3A7B97F6BD1646` FOREIGN KEY (`site_id`) REFERENCES `Site` (`id`);

--
-- Contraintes pour la table `Image`
--
ALTER TABLE `Image`
  ADD CONSTRAINT `FK_4FC2B5B29C1004E` FOREIGN KEY (`video_id`) REFERENCES `Video` (`id`),
  ADD CONSTRAINT `FK_4FC2B5B7294869C` FOREIGN KEY (`article_id`) REFERENCES `Article` (`id`);

--
-- Contraintes pour la table `Publi`
--
ALTER TABLE `Publi`
  ADD CONSTRAINT `FK_14A58A61C4663E4` FOREIGN KEY (`page_id`) REFERENCES `Page` (`id`),
  ADD CONSTRAINT `FK_14A58A617294869C` FOREIGN KEY (`article_id`) REFERENCES `Article` (`id`);

--
-- Contraintes pour la table `Stats`
--
ALTER TABLE `Stats`
  ADD CONSTRAINT `FK_968648AE9502F0B` FOREIGN KEY (`stat_id`) REFERENCES `Page` (`id`),
  ADD CONSTRAINT `FK_968648AE7294869C` FOREIGN KEY (`article_id`) REFERENCES `Article` (`id`);

--
-- Contraintes pour la table `Video`
--
ALTER TABLE `Video`
  ADD CONSTRAINT `FK_BD06F5287294869C` FOREIGN KEY (`article_id`) REFERENCES `Article` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
// app/config/parameters.php
$container->setParameter('database_driver', 'pdo_mysql');
if(@$_SERVER['RDS_HOSTNAME'])
{
    $container->setParameter('database_host', $_SERVER['RDS_HOSTNAME']);
    $container->setParameter('database_port', $_SERVER['RDS_PORT']);
    $container->setParameter('database_name', $_SERVER['RDS_DB_NAME']);
    $container->setParameter('database_user', $_SERVER['RDS_USERNAME']);
    $container->setParameter('database_password', $_SERVER['RDS_PASSWORD']);
    $container->setParameter('amazon_s3_base_url', $_SERVER['AMAZON_S3_BASE_URL']);
    $container->setParameter('amazon_s3_bucket_name', $_SERVER['AMAZON_S3_BUCKET_NAME']);
    $container->setParameter('amazon_aws_region', $_SERVER['AMAZON_AWS_REGION']);




} else {
    include ("parameters.local.php");
}

$container->setParameter('mailer_transport', 'smtp');
$container->setParameter('mailer_host', '127.0.0.1');
$container->setParameter('mailer_password', 'root');
$container->setParameter('mailer_user', null);
$container->setParameter('secret', 'ThisTokenIsNotSoSecretChangeIt');

$container->setParameter('amazon_s3_bucket_name', 'image-myfunnybuzz');

